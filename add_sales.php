<?php
  $page_title = 'DBEI | Product codes';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
  
  $tproducts = join_product_name_table();
  $all_products = find_all('products');
  $all_customers = find_all('customers');
  // $all_salesmans = find_all('salesman');
  $employees = find_all('employees');
  $all_tsales = find_all('product_cart')
?>

<?php
 if(isset($_POST['temp_product'])){
   $req_field = array('prod-name');
   validate_fields($req_field);
      $prod_name = remove_junk($db->escape($_POST['prod-name']));
      $name = remove_junk($db->escape($_POST['name']));
      $price = remove_junk($db->escape($_POST['price']));

   if(empty($errors)){

      $sql  = "INSERT INTO product_cart (product_id, name, sale_price)";
      $sql .= " VALUES ('{$prod_name}','{$name}','{$price}')";

      if($db->query($sql)){
        $session->msg("s", "Product add to list successfully.");
        redirect('add_sales.php',false);
      } else {
        $session->msg("d", "Sorry! failed to add to list.");
        redirect('add_sales.php',false);
      }
   } else {
     $session->msg("d", $errors);
     redirect('add_sales.php',false);
   }
 }
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
  <div class="row">
     <div class="col-md-3 pull-right noti">
       <?php echo display_msg($msg); ?>
      </div>
  </div>
  <div class="row">
     <ol class="breadcrumb pull-right">
        <li><a href="admin.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Add Sale</li>
      </ol>
    <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Add Sales</b></p>
    <div class="col-md-3">
      <div class="panel panel-default">
        <div class="panel-body">
          <form method="post" action="add_sales.php">
            <div class="form-group col-sm-12">
                <select class="form-control input-sm select_products" name="prod-name" onchange="showUser(this.value)">
                  <option disabled selected>Products</option>
                    <?php  foreach ($all_products as $product): ?>
                      <option value="<?php echo $product['id'] ?>">
                      <?php echo $product['name'] ?></option>
                    <?php endforeach; ?>
                </select><br>
                <div id="txtHint"></div>
            </div>
            <button type="submit" name="temp_product" class="btn btn-danger btn-sm select_prdct pull-right" disabled>Select Product</button>
            <!-- <button type="button" name="clear_product" class="btn btn-default clear_prdct pull-right">Clear Product</button> -->
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-9 selected_prdct">
      <div class="panel panel-default">
        <div class="panel-heading col-md-8"> 
          <div class="form-group pull-left">
            <input placeholder="Sale Invoice No." type="number" class="form-control input-sm">
          </div>
          <div class="form-group pull-left">&nbsp;&nbsp;&nbsp;</div>
            <div class="form-group pull-left">
                <select class="form-control input-sm">
                  <option>Payment Term</option>
                  <option>Cash</option>
                  <option>Credit</option>
              </select>
           </div>
            <div class="form-group pull-right">
              <input placeholder="Date" type="text" class="form-control input-sm datepicker"> 
            </div>
          </div>
        <div class="panel-body">
          <div class="col-sm-3 pull-right input-group">
            <input type="text" class="form-control input-sm" id="search" placeholder="Search a products here..">
              <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
          </div>
          <table class="table table-bordered table-condensed">
            <thead>
              <tr>
                <th class="text-center" style="width: 50px;">#</th>
                <th>QTY</th>
                <th>Product</th>
                <th>Price</th>                     
                <th>Routes</th>
                <th>Customers</th>
                <th>Total</th>
                <th class="text-center" style="width: 100px;">Actions</th>
              </tr>
            </thead>
            <tbody class="tablesearch">
              <?php foreach ($tproducts as $tproduct):?>
                <tr id="product_info">
                  <td class="text-center"><?php echo count_id();?></td>
                  <td style="width: 12%;"><input class="form-control input-sm qaulity" name="quantity" value="0"></td>
                  <td style="width: 19%;" class="code"><?php echo remove_junk(ucfirst($tproduct['name'])); ?></td>
                  <td><input name="price" class="form-control input-sm" value="<?php echo remove_junk(ucfirst($tproduct['sale_price'])); ?>"></td>
                  <td style="width: 19%;">
                    <select class="form-control input-sm" name="channels">
                        <option disabled selected>-</option>
                          <?php  foreach ($employees as $employee): ?>
                            <option value="<?php echo $employee['id'] ?>">
                            <?php echo $employee['name'] ?></option>
                          <?php endforeach; ?>
                    </select>
                  </td>
                  <td>
                    <select class="form-control input-sm" name="channels">
                        <option disabled selected>-</option>
                          <?php  foreach ($all_customers as $customer): ?>
                            <option value="<?php echo $customer['id'] ?>">
                            <?php echo $customer['name'] ?></option>
                          <?php endforeach; ?>
                    </select>
                  </td>
                  <td style="width: 10%;"><input name="total" class="form-control input-sm" value="0"></td>
                  <td class="text-center">
                    <div class="btn-group">
                      <a href="delete_tsale.php?id=<?php echo (int)$tproduct['id'];?>"  title="Remove" data-toggle="tooltip">
                      &nbsp;&nbsp;<i class="glyphicon glyphicon-trash"></i>&nbsp;&nbsp;
                      </a>
                    </div>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
       </div>
      </div>
    <div>
    </div>
      <p class="pull-right"><b>Total</b><input class="form-control input-sm" value="0" readonly style="cursor:not-allowed;"><br>
      <button type="submit" name="make_sale" class="btn btn-danger btn-sm pull-right">Make Sale </button></p>   
      <br><br><br>
    </div>
   </div>
   <?php include_once('layouts/code_modal.php'); ?>
  </div>
  <?php include_once('layouts/footer.php'); ?>
  <?php include_once('includes/searchjs.php'); ?>

<script>
  function total(){
    $('#product_info input').change(function(e)  {
            var price = +$('input[name=price]' ).val() || 0;
            var qty   = +$('input[name=quantity]').val() || 0;
            var total = qty * price ;
                $('input[name=total]').val(total.toFixed(2));
                //alert('adad');
       $('.select_products').prop('disabled',true);       
    });
  }

   $(document).ready(function(){
    // Callculate total ammont
    total();

      $('.clear_prdct').hide(); //first hide the clear button on load.
      $('.select_products').change(function(){ //
         $('.select_prdct').prop('disabled',false);
         $('.clear_prdct').hide();
      });
      // show list right side for selected products
      $(".select_prdct").submit( function(){
        $(".selected_prdct").removeClass('hidden');
        $(".select_products").prop('disabled',true);
        $(".select_prdct").prop('disabled',true);
        $('.clear_prdct').show();
      });
      // clear selected products on left side
      // $(".clear_prdct").click( function(){
      //   $(".selected_prdct").addClass('hidden');
      //   $(".select_products").prop('disabled',false);
      //   $(".select_prdct").prop('disabled',false);
      //   $(".table_summary").hide();
      // });

  });

  function showUser(str) {
    if (str == "") {
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","getuser.php?q="+str,true);
        xmlhttp.send();
    }
}
</script>
