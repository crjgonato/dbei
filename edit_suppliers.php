<?php
  $page_title = 'DBEI | Edit Suppliers';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
?>
<?php
  //Display all suppliers.
  $supplier = find_by_id('supplier',(int)$_GET['id']);
  if(!$supplier){
    $session->msg("d","Missing code id.");
    redirect('supplier.php');
  }
?>
<?php
if(isset($_POST['edit_supplier'])){
  $req_field = array('supplier');
  validate_fields($req_field);
  $supplier_name = remove_junk($db->escape($_POST['supplier']));
  $address = remove_junk($db->escape($_POST['address']));
  $contacts = remove_junk($db->escape($_POST['contacts']));
  $vatregtine = remove_junk($db->escape($_POST['vatregtine']));
  $date = make_date();
  if(empty($errors)){
        $sql = "UPDATE supplier SET name='{$supplier_name}',address='{$address}',contacts='{$contacts}',vatregtine='{$vatregtine}', date_mod='{$date}'";
       $sql .= " WHERE id='{$supplier['id']}'";
     $result = $db->query($sql);
     if($result && $db->affected_rows() === 1) {
       $session->msg("s", "Supplier updated succesfully.");
       redirect('supplier.php',false);
     } else {
       $session->msg("d", "Sorry! failed to update.");
       redirect('supplier.php',false);
     }
  } else {
    $session->msg("d", $errors);
    redirect('supplier.php',false);
  }
}
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
  <div class="row">
     <div class="col-md-3 pull-right noti">
       <?php echo display_msg($msg); ?>
    </div>
  </div>
   <div class="row">
   <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Edit Supplier</b></p>
    <div class="col-md-3">
      <div class="panel panel-default">
        <div class="panel-body">
          <form method="post" action="edit_supplier.php?id=<?php echo (int)$supplier['id'];?>">
           <div class="form-group">
           <p>Supplier name</p>
               <input type="text" class="form-control input-sm supplier" name="supplier" value="<?php echo remove_junk(ucfirst($supplier['name']));?>">
           </div>
           <div class="form-group">
           <p>Address</p>
               <input type="text" class="form-control input-sm supplier" name="address" value="<?php echo remove_junk(ucfirst($supplier['address']));?>">
           </div>
           <div class="form-group">
           <p>Contacts</p>
               <input type="text" class="form-control input-sm supplier" name="contacts" value="<?php echo remove_junk(ucfirst($supplier['contacts']));?>">
           </div>
           <div class="form-group">
           <p>VAT Reg TIN</p>
               <input type="text" class="form-control input-sm supplier" name="vatregtine" value="<?php echo remove_junk(ucfirst($supplier['vatregtine']));?>">
           </div>
           <button type="button" name="cancel" class="btn btn-default btn-sm pull-left" onclick="goBack();">Cancel</button>
           <button type="submit" name="edit_supplier" class="btn btn-danger btn-sm btnupdate pull-right" disabled>Apply Changes</button>
       </form>
        </div>
      </div>
    </div>
   </div>
   <?php include_once('layouts/code_modal.php'); ?>
  </div>
  <?php include_once('layouts/footer.php'); ?>
  <?php include_once('includes/searchjs.php'); ?>
  <script>
  $().ready(function() {
    //disbaled update button after any changes above inputs
    //
    $(".supplier").focus( function(){
        $(".btnupdate").prop('disabled',false);
    });
  });
</script>

