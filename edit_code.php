<?php
  $page_title = 'DBEI | Edit code';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
?>
<?php
  //Display all catgories.
  $code = find_by_id('codes',(int)$_GET['id']);
  if(!$code){
    $session->msg("d","Missing code id.");
    redirect('code.php');
  }
?>

<?php
if(isset($_POST['edit_code'])){
  $req_field = array('product-code');
  validate_fields($req_field);
  $code_name = remove_junk($db->escape($_POST['product-code']));
  $date = make_date();
  if(empty($errors)){
        $sql = "UPDATE codes SET name='{$code_name}',date_mod='{$date}'";
       $sql .= " WHERE id='{$code['id']}'";
     $result = $db->query($sql);
     if($result && $db->affected_rows() === 1) {
       $session->msg("s", "Code updated succesfully.");
       redirect('code.php',false);
     } else {
       $session->msg("d", "Sorry! failed to update.");
       redirect('code.php',false);
     }
  } else {
    $session->msg("d", $errors);
    redirect('code.php',false);
  }
}
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
<div class="row">
<div class="col-md-3 pull-right noti">
<?php echo display_msg($msg); ?>
</div><br>
<p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Edit Product Code</b></p>
   <div class="col-md-3">
     <div class="panel panel-default">
       <div class="panel-body">
         <form method="post" action="edit_code.php?id=<?php echo (int)$code['id'];?>">
           <div class="form-group">
           <p>Code name</p>
               <input type="text" class="form-control input-sm code" name="product-code" value="<?php echo remove_junk(ucfirst($code['name']));?>">
           </div>
          <button type="button" name="cancel" class="btn btn-default btn-sm pull-left" onclick="goBack();">Cancel</button>
           <button type="submit" name="edit_code" class="btn btn-danger btn-sm btnupdate pull-right" disabled>Apply Changes</button>
       </form>
       </div>
     </div>
   </div>
</div>



<?php include_once('layouts/footer.php'); ?>
<script>
  $(document).ready(function() {
    //disbaled update button after any changes above inputs
    //
    $(".code").focus( function(){
        $(".btnupdate").prop('disabled',false);
    });
  });
</script>
