<?php
  $page_title = 'DBEI | Sales';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
   page_require_level(3);
?>
<?php
$sales = find_all_sale();
//$employees = find_all('employees');
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
<div class="row">
<div class="col-md-3 pull-right noti">
<?php echo display_msg($msg); ?>
</div>
</div>
  <div class="row">
  <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Sales</b></p>
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <strong>
            <!-- <span class="glyphicon glyphicon-th"></span> -->
            <!-- <span>All Sales</span> -->
          </strong>
          <div class="col-sm-4 pull-left input-group">
            <input type="text" class="form-control" id="search" placeholder="Search sales here.." autofocus>
              <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
          </div>
          <div class="pull-right">
            <a href="add_sales.php" class="btn btn-danger">Add sale</a>
          </div>
        </div>
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <!-- <th class="text-center">#</th> -->
                <th class="text-center" style="width: 10%;"> Date </th>
                <th> SI No. </th>
                <th> Customers </th>
                <th> Product </th>
                <th> Salesman </th>
                
                <!-- <th> Price </th> -->
                <th class="text-center"> Quantity</th>
                <th class="text-center"> Total </th>
                <th> Payment </th>
                
                <th class="text-center" style="width: 15%;"> Actions </th>
             </tr>
            </thead>
           <tbody class="tablesearch">
             <?php foreach ($sales as $sale):?>
             <tr>
               <!-- <td class="text-center"><//?php echo count_id();?></td> -->
                <td class="text-center"><?php echo $sale['date']; ?></td>
               <td><?php echo remove_junk($sale['sales_invoice']); ?></td>
                <td><?php echo remove_junk($sale['customer']); ?></td>
               <td><?php echo remove_junk($sale['name']); ?></td>
               <td><?php echo remove_junk($sale['salesman']); ?></td>
              
               <!-- <td><//?php echo remove_junk($sale['price']); ?></td> -->
               <td class="text-center"><?php echo ($sale['qty']); ?></td>
               <td class="text-center"><?php echo remove_junk($sale['price']); ?></td>
               <td><?php echo remove_junk($sale['term']); ?></td>
              
               <td class="text-center">
                  <div class="btn-group">
                     <a href="edit_sale.php?id=<?php echo (int)$sale['id'];?>" title="Edit" data-toggle="tooltip">
                       &nbsp;&nbsp;<i class="glyphicon glyphicon-edit"></i>&nbsp;&nbsp;
                     </a>
                     <!-- <a href="delete_sale.php?id=<?php echo (int)$sale['id'];?>" class="btn btn-danger btn-xs"  title="Remove" data-toggle="tooltip">
                     &nbsp;&nbsp;<i class="glyphicon glyphicon-trash"></i>&nbsp;&nbsp;
                     </a> -->
                  </div>
               </td>
             </tr>
             <?php endforeach;?>
           </tbody>
         </table>
        </div>
      </div>
    </div>
    <?php include_once('layouts/sale_modal.php'); ?>
  </div>
<?php include_once('layouts/footer.php'); ?>
<?php include_once('includes/searchjs.php'); ?>
