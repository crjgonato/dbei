<?php
  $page_title = 'DBEI |  Logs';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
  
  $all_logs = find_logs();
?>

<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
  <div class="row">
     <div class="col-md-3 pull-right noti">
       <?php echo display_msg($msg); ?>
    </div>
  </div>
   <div class="row">
      <ol class="breadcrumb pull-right">
        <li><a href="admin.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Logs</li>
      </ol>
   <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Logs</b></p>    
    <div class="col-md-10">
    <div class="panel panel-default">
        <div class="panel-body">
          <div class="col-md-4 pull-right input-group">
            <input type="text" class="form-control input-sm" id="search" placeholder="Search a logs here..">
              <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
          </div>
          <table class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th class="text-center" style="width: 50px;">#</th>
                    <th>Logs</th>
                    <th>Actions</th>
                    <th>Dates</th>
                </tr>
            </thead>
            <tbody class="tablesearch">
              <?php foreach ($all_logs as $logs):?>
                <tr>
                    <td class="text-center"><?php echo count_id();?></td>
                    <td><?php echo remove_junk(ucfirst($logs['name'])); ?></td>
                    <td><?php echo remove_junk(ucfirst($logs['action'])); ?></td>
                    <td><?php echo read_date(($logs['logs_date'])); ?></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
       </div>
    </div>
    </div>
   </div>
   <?php include_once('layouts/code_modal.php'); ?>
  </div>
  <?php include_once('layouts/footer.php'); ?>
  <?php include_once('includes/searchjs.php'); ?>

