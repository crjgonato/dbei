<?php
  require_once('includes/load.php');
  if (!$session->isUserLoggedIn(true)) { redirect('index.php', false);}
?>
<?php
// Checkin What level user has permission to view this page
 page_require_level(3);
 //page_require_level(3);
?>

<?php
 // Auto suggetion
    $html = '';
   if(isset($_POST['product_name']) && strlen($_POST['product_name']))
   {
     $products = find_product_by_title($_POST['product_name']);
     if($products){
        foreach ($products as $product):
           $html .= "<li class=\"list-group-item\">";
           $html .= $product['name'];
           $html .= "</li>";
         endforeach;
      } else {

        $html .= '<li onClick=\"fill(\''.addslashes().'\')\" class=\"list-group-item\">';
        $html .= 'No Product Found ';
        $html .= "</li>";

      }

      echo json_encode($html);
   }
 ?>
 <?php
 // find all product
  if(isset($_POST['p_name']) && strlen($_POST['p_name']))
  {
    $product_title = remove_junk($db->escape($_POST['p_name']));
    if($results = find_all_product_info_by_title($product_title)){
        foreach ($results as $result) {

          $html .= "<tr>";

          $html .= "<td style='width: 11%;'><input type=\"text\" name=\"sales_invoice\" class=\"form-control\"></td>";
          $html .= "<td  style='width: 15%;' id=\"s_name\">".$result['name']."</td>";
          $html .= "<input type=\"hidden\" name=\"s_id\" value=\"{$result['id']}\">";
          
          
          $html .="<td  style='width: 8%;'>
                      <select class=\"form-control\" name=\"customer\">
                        <option value=\"R01\">R01</option>
                        <option value=\"R02\">R02</option>
                     </select>
                  </td>";
          

          $html .="<td  style='width: 12%;'>
                      <select class=\"form-control\" name=\"customer\">
                        <option value=\"Customer 1\">Customer 1</option>
                        <option value=\"Customer 2\">Customer 2</option>
                      </select>
                  </td>";
        
          $html  .= "<td style='width: 9%;'>";
          $html  .= "<input type=\"text\" class=\"form-control\" name=\"price\" value=\"{$result['sale_price']}\">";
          $html  .= "</td>";
          $html .= "<td  style='width: 9%;' id=\"s_qty\">";
          $html .= "<input type=\"text\" class=\"form-control\" name=\"quantity\" value=\"1\">";
          $html  .= "</td>";
          $html  .= "<td style='width: 9%;'>";
          $html  .= "<input type=\"text\" class=\"form-control\" name=\"total\" value=\"{$result['sale_price']}\">";
          $html  .= "</td>";
          //$html .= "<td><input id=\"s_term\" type=\"text\" name=\"term\"></td>";
          $html .="<td style='width: 9%;'>
                      <select class=\"form-control\" name=\"term\">
                        <option value=\"Credit\">Credit</option>
                        <option value=\"Cash\">Cash</option>
                      </select>
                    </td>";
          $html  .= "<td style='width: 5%;'>";
          $html  .= "<input type=\"date\" class=\"form-control datePicker\" name=\"date\" data-date data-date-format=\"yyyy-mm-dd\">";
          $html  .= "</td>";
          $html  .= "<td>";
          $html  .= "<button type=\"submit\" name=\"add_sale\" class=\"btn btn-danger\">Make Sale</button>";
          $html  .= "</td>";
          $html  .= "</tr>";

        }
        
    } else {
        $html ='<tr><td>Product Name not resgister in our records</td></tr>';
    }

    echo json_encode($html);
  }
 ?>
