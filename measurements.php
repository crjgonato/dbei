<?php
  $page_title = 'DBEI | Units';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
  
  $units = find_uom();
?>
<?php
 if(isset($_POST['add_units'])){
   $req_field = array('units');
   validate_fields($req_field);
   $unit_measure = remove_junk($db->escape($_POST['units']));
   $date = make_date();
   if(empty($errors)){
      $sql  = "INSERT INTO unit_measures (name,date)";
      $sql .= " VALUES ('{$unit_measure}','{$date}')";
      if($db->query($sql)){
        $session->msg("s", "Unit created successfully.");
        redirect('measurements.php',false);
      } else {
        $session->msg("d", "Sorry! failed to create.");
        redirect('measurements.php',false);
      }
   } else {
     $session->msg("d", $errors);
     redirect('measurements.php',false);
   }
 }
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
  <div class="row">
  <div class="col-md-3 pull-right noti">
  <?php echo display_msg($msg); ?>
</div>
  </div>
   <div class="row">
     <ol class="breadcrumb pull-right">
        <li><a href="admin.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Unit of Measurements</li>
      </ol>
   <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Unit of Measurements</b></p>
    <div class="col-md-3">
      <div class="panel panel-default">
        <div class="panel-body">
          <form method="post" action="measurements.php">
            <div class="form-group">
                <input type="text" class="form-control input-sm initial_input" name="units" placeholder="New Unit" autofocus require="required"> 
            </div>
            <button type="submit" name="add_units" class="btn btn-danger btn-sm pull-right">Add Unit</button>
        </form>
        </div>
      </div>
    </div>
    <div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-body">
        <div class="col-sm-4 pull-right input-group">
            <input type="text" class="form-control input-sm" id="search" placeholder="Search unit here..">
              <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
          </div>
          <table class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th class="text-center" style="width: 50px;">#</th>
                    <th>Unit Names</th>
                     <th>Date Added</th>
                    <th class="text-center" style="width: 100px;">Actions</th>
                </tr>
            </thead>
            <tbody class="tablesearch">
              <?php foreach ($units as $unit):?>
                <tr>
                    <td class="text-center"><?php echo count_id();?></td>
                    <td><?php echo remove_junk(ucfirst($unit['name'])); ?></td>
                    <td><?php echo remove_junk(ucfirst($unit['date'])); ?></td>
                    <td class="text-center">
                      <div class="btn-group">
                        <a href="edit_measurements.php?id=<?php echo (int)$unit['id'];?>" data-toggle="tooltip" title="Edit">
                          &nbsp;&nbsp;<i class="glyphicon glyphicon-edit"></i>&nbsp;&nbsp;
                        </a>
                        <!-- <a href="delete_measurements.php?id=<//?php echo (int)$unit['id'];?>"  class="btn btn-xs btn-danger" title="Remove" data-toggle="tooltip">
                        &nbsp;&nbsp;<i class="glyphicon glyphicon-trash"></i>&nbsp;&nbsp;
                        </a> -->
                      </div>
                    </td>

                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
       </div>
    </div>
    </div>
   </div>
   <?php include_once('layouts/measure_modal.php'); ?>
  </div>
  <?php include_once('layouts/footer.php'); ?>
  <?php include_once('includes/searchjs.php'); ?>