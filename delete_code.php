<?php
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
?>
<?php
  $codes = find_by_id('codes',(int)$_GET['id']);
  if(!$codes){
    $session->msg("d","Missing Code id.");
    redirect('code.php');
  }
?>
<?php
  $delete_id = delete_by_id('codes',(int)$codes['id']);
  if($delete_id){
      $session->msg("s","Code has been deleted.");
      redirect('code.php');
  } else {
      $session->msg("d","Code deletion failed.");
      redirect('code.php');
  }
?>
