<?php
  $page_title = 'DBEI | Suppliers';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
  
  $all_supplier = find_supplier();
?>
<?php
 if(isset($_POST['add_supplier'])){
   $req_field = array('supplier');
   validate_fields($req_field);
      $supplier = remove_junk($db->escape($_POST['supplier']));
      $address = remove_junk($db->escape($_POST['address']));
      $contacts = remove_junk($db->escape($_POST['contacts']));
      $vatregtine = remove_junk($db->escape($_POST['vatregtine']));
      $date = make_date();
   if(empty($errors)){
      $sql  = "INSERT INTO supplier (name,address,contacts,vatregtine,date)";
      $sql .= " VALUES ('{$supplier}','{$address}','{$contacts}','{$vatregtine}','{$date}')";

      if($db->query($sql)){
        $session->msg("s", "Supplier created successfully.");
        redirect('supplier.php',false);
      } else {
        $session->msg("d", "Sorry! failed to create.");
        redirect('supplier.php',false);
      }
   } else {
     $session->msg("d", $errors);
     redirect('supplier.php',false);
   }
 }
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
  <div class="row">
     <div class="col-md-3 pull-right noti">
       <?php echo display_msg($msg); ?>
    </div>
  </div>
   <div class="row">
     <ol class="breadcrumb pull-right">
        <li><a href="admin.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Supplier</li>
      </ol>
   <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Supplier</b></p>
    <div class="col-md-3">
      <div class="panel panel-default">
        <div class="panel-body">
          <form method="post" action="supplier.php">
            <div class="form-group">
                <input type="text" class="form-control input-sm" name="supplier" placeholder="New Supplier Name" autofocus> 
            </div>
            <div class="form-group">
                <input type="text" class="form-control input-sm" name="address" placeholder="Address" autofocus> 
            </div>
            <div class="form-group">
                <input type="text" class="form-control input-sm" name="contacts" placeholder="Contacts" autofocus> 
            </div>
            <div class="form-group">
                <input type="text" class="form-control input-sm" name="vatregtine" placeholder="VAT REG TIN" autofocus> 
            </div>
            <button type="submit" name="add_supplier" class="btn btn-danger btn-sm pull-right">Add Supplier</button>
        </form>
        </div>
      </div>
    </div>
    <div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-body">
          <div class="col-sm-4 pull-right input-group">
            <input type="text" class="form-control input-sm" id="search" placeholder="Search supplier here..">
              <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
          </div>
          <table class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th class="text-center" style="width: 50px;">#</th>
                    <th>Supplier Names</th>
                    <th>Address</th>
                    <th>Contacts</th>
                    <th>VAT Reg TIN</th>
                    <th class="text-center" style="width: 100px;">Actions</th>
                </tr>
            </thead>
            <tbody class="tablesearch">
              <?php foreach ($all_supplier as $supplier):?>
                <tr>
                    <td class="text-center"><?php echo count_id();?></td>
                    <td><?php echo remove_junk(ucfirst($supplier['name'])); ?></td>
                    <td><?php echo remove_junk(ucfirst($supplier['address'])); ?></td>
                    <td><?php echo remove_junk(ucfirst($supplier['contacts'])); ?></td>
                    <td><?php echo remove_junk(ucfirst($supplier['vatregtine'])); ?></td>
                    <td class="text-center">
                      <div class="btn-group">
                        <a href="edit_suppliers.php?id=<?php echo (int)$supplier['id'];?>" data-toggle="tooltip" title="Edit">
                          &nbsp;&nbsp;<i class="glyphicon glyphicon-edit"></i>&nbsp;&nbsp;
                        </a>
                        
                        <!-- <a href="#id=<//?php echo (int)$codes['id'];?>"  class="btn btn-xs btn-danger" title="Remove" data-toggle="modal" data-target="#myModal">
                          <span class="glyphicon glyphicon-trash"></span>
                        </a> -->

                        <!-- <a href="delete_code.php?id=<?php echo (int)$supplier['id'];?>"  class="btn btn-xs btn-danger" title="Remove" data-toggle="tooltip">
                        &nbsp;&nbsp;<i class="glyphicon glyphicon-trash"></i>&nbsp;&nbsp;
                        </a> -->
                      </div>
                    </td>

                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
       </div>
    </div>
    </div>
   </div>
   <?php include_once('layouts/code_modal.php'); ?>
  </div>
  <?php include_once('layouts/footer.php'); ?>
  <?php include_once('includes/searchjs.php'); ?>

