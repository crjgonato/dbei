<?php
  $page_title = 'DBEI | Salesman';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
  
  $all_salesman = find_salesman();
  $employees = find_all('employees');
?>
<?php
 if(isset($_POST['add_man'])){
   $req_field = array('salesman','platenumber');
   validate_fields($req_field);
      $sales_man = remove_junk($db->escape($_POST['salesman']));
      $platenumber = remove_junk($db->escape($_POST['platenumber']));
      $date = make_date();
   if(empty($errors)){
     
       $sql  = "INSERT INTO salesman (";
        $sql .="employee_id,platenumber,date";
        $sql .=") VALUES (";
        $sql .="'{$sales_man}','{$platenumber}','{$date}'";
        $sql .=")";

      if($db->query($sql)){
        $session->msg("s", "Salesman created successfully.");
        redirect('salesman.php',false);
      } else {
        $session->msg("d", "Sorry! failed to create.");
        redirect('salesman.php',false);
      }
   } else {
     $session->msg("d", $errors);
     redirect('salesman.php',false);
   }
 }
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180"/>
  <div class="row">
     <div class="col-md-3 pull-right noti">
       <?php echo display_msg($msg); ?>
    </div>
  </div>
   <div class="row">
     <ol class="breadcrumb pull-right">
        <li><a href="admin.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Salesman</li>
      </ol>
   <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Salesman</b></p>
    <div class="col-md-3">
      <div class="panel panel-default">
        <div class="panel-body">
          <form method="post" action="salesman.php">
            <div class="form-group">
                <!-- <input type="text" class="form-control input-sm" name="salesman" placeholder="New Salesman" autofocus>  -->
                <select class="form-control input-sm" name="salesman">
                  <option selected>New Salesman</option>
                  <?php foreach ($employees as $employee ):?>
                   <option value="<?php echo $employee['id'];?>"><?php echo ucwords($employee['name']);?></option>
                <?php endforeach;?>
                </select>
            </div>
            <div class="form-group">
                <input type="text" class="form-control input-sm" name="platenumber" placeholder="Plate Number" autofocus> 
            </div>
            <button type="submit" name="add_man" class="btn btn-danger btn-sm pull-right">Add Salesman</button>
        </form>
        </div>
      </div>
    </div>
    <div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-body">
          <div class="col-sm-4 pull-right input-group">
            <input type="text" class="form-control input-sm" id="search" placeholder="Search salesman here..">
              <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
          </div>
          <table class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th class="text-center" style="width: 50px;">#</th>
                    <th>Salesman</th>
                    <th>Plate No.</th>
                    <th class="text-center" style="width: 100px;">Actions</th>
                </tr>
            </thead>
            <tbody class="tablesearch">
              <?php foreach ($all_salesman as $man):?>
                <tr>
                    <td class="text-center"><?php echo count_id();?></td>
                    <td class="code"><?php echo remove_junk(ucfirst($man['name'])); ?></td>
                    <td class="code"><?php echo remove_junk(ucfirst($man['platenumber'])); ?></td>
                    <td class="text-center">
                      <div class="btn-group">
                        <a href="edit_salesmans.php?id=<?php echo (int)$man['id'];?>"data-toggle="tooltip" title="Edit">
                          &nbsp;&nbsp;<i class="glyphicon glyphicon-edit"></i>&nbsp;&nbsp;
                        </a>
                        
                      </div>
                    </td>

                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
       </div>
    </div>
    </div>
   </div>
   <?php include_once('layouts/code_modal.php'); ?>
  </div>
  <?php include_once('layouts/footer.php'); ?>
  <?php include_once('includes/searchjs.php'); ?>

