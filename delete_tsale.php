<?php
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
?>
<?php
  $product = find_by_id('product_cart',(int)$_GET['id']);
  if(!$product){
    $session->msg("d","Missing product id.");
    redirect('add_sales.php');
  }
?>
<?php
  $delete_id = delete_by_id('product_cart',(int)$product['id']);
  if($delete_id){
      $session->msg("s","product has been deleted.");
      redirect('add_sales.php');
  } else {
      $session->msg("d","product deletion failed.");
      redirect('add_sales.php');
  }
?>
