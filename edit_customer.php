<?php
  $page_title = 'DBEI | Edit customers';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
  
  //$all_customers = find_all('customers');
?>
<?php
  //Display all catgories.
  $customer = find_by_id('customers',(int)$_GET['id']);
  $all_channels = find_all('channels');
  if(!$customer){
    $session->msg("d","Missing customer id.");
    redirect('customers.php');
  }
?>
<?php
 if(isset($_POST['add_customer'])){
    $req_field = array('codename','businessname','name','gender','channels','balance','contact','zipcode','street','brgy','city');
    validate_fields($req_fields);

   if(empty($errors)){
        $c_codename = remove_junk($db->escape($_POST['codename']));
        $c_businessname = remove_junk($db->escape($_POST['businessname']));
        $c_name = remove_junk($db->escape($_POST['name']));
        $c_gender = remove_junk($db->escape($_POST['gender']));
        //$c_channels = remove_junk($db->escape($_POST['channels']));
        $c_channels   = (int)$_POST['channels'];
        $c_balance = remove_junk($db->escape($_POST['balance']));
        $c_contact = remove_junk($db->escape($_POST['contact']));
        $c_zipcode = remove_junk($db->escape($_POST['zipcode']));
        $c_street = remove_junk($db->escape($_POST['street']));
        $c_brgy = remove_junk($db->escape($_POST['brgy']));
        $c_city = remove_junk($db->escape($_POST['city']));
        $date = make_date();

       if (is_null($_POST['product-photo']) || $_POST['product-photo'] === "") {
         $media_id = '0';
       } else {
         $media_id = remove_junk($db->escape($_POST['product-photo']));
       }

       $query   = "UPDATE customers SET";
       $query  .=" codename ='{$c_codename}', businessname ='{$c_businessname}',";
       $query  .=" name ='{$c_name}', gender ='{$c_gender}', channels_id ='{$c_channels}', balance ='{$c_balance}', contact ='{$c_contact}', zipcode='{$c_zipcode}', street='{$c_street}', brgy='{$c_brgy}', city='{$c_city}', date_mod='{$date}'";
       $query  .=" WHERE id ='{$customer['id']}'";
       $result = $db->query($query);
               if($result && $db->affected_rows() === 1){
                 $session->msg('s',"Customer updated succesfully.");
                 redirect('customers.php', false);
               } else {
                 $session->msg("d", "Sorry! failed to update.");
                 redirect('edit_customer.php?id='.$customer['id'], false);
               }

   } else{
       $session->msg("d", $errors);
       redirect('edit_customer.php?id='.$customer['id'], false);
   }

 }

?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
  <div class="row">
  <div class="col-md-3 pull-right noti">
  <?php echo display_msg($msg); ?>
</div>
  </div>
   <div class="row">
   <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Edit Customer</b></p>
    <div class="col-md-3">
      <div class="panel panel-default">
        <div class="panel-body">
          <form method="post" action="edit_customer.php?id=<?php echo (int)$customer['id'];?>">
          <div class="form-group">
            <p>Customer Code</p>
                <input type="text" class="form-control input-sm cust_code" name="codename" placeholder="Customer Code" value="<?php echo remove_junk(ucfirst($customer['codename']));?>"> 
            </div>
             <div class="form-group">
             <p>Business Name</p>
                <input type="text" class="form-control input-sm cust_bizname" name="businessname" placeholder="Business Name" value="<?php echo remove_junk(ucfirst($customer['businessname']));?>"> 
            </div>

            <div class="form-group">
            <p>Name</p>
                <input type="text" class="form-control input-sm cust_name" name="name" placeholder="Name" value="<?php echo remove_junk(ucfirst($customer['name']));?>"> 
            </div>
            
           <div class="form-group ">
              <div class="form-group col-sm-5">
              <p>Gender</p>
                    <select class="form-control input-sm cust_gender" name="gender">
                        <!-- <option disabled>Gender</option> -->
                        <option value="<?php echo remove_junk(ucfirst($customer['gender']));?>" selected><?php echo remove_junk(ucfirst($customer['gender']));?></option>
                        
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                </div>
                <div class="form-group col-sm-7">
                 <p>Channel</p>
                <select class="form-control input-sm cust_channel" name="channels">
                         <?php  foreach ($all_channels as $channels): ?>
                              <option value="<?php echo (int)$channels['id']; ?>" <?php if($customer['channels_id'] === $channels['id']): echo "selected"; endif; ?> >
                              <?php echo remove_junk($channels['name']); ?></option>
                        <?php endforeach; ?>
                    </select> 
                   
                </div>
           </div>
            <div class="form-group">
             <p>Contact No.</p>
                <input type="text" class="form-control input-sm cust_no" name="contact" placeholder="Contact Number" value="<?php echo remove_junk(ucfirst($customer['contact']));?>"> 
            </div>
            <div class="form-group">
             <p>Balance</p>
                <input type="text" class="form-control input-sm" name="balance" placeholder="Payment Balance" value="<?php echo remove_junk(ucfirst($customer['balance']));?>" readonly style="cursor:not-allowed;"> 
            </div>
           <fieldset>
           <legend><h5><b>Address</b></h5></legend>
           <div class="form-group">
           <p>Zipcode</p>
                    <input type="text" class="form-control input-sm cust_zipcode" name="zipcode" placeholder="ZipCode" value="<?php echo remove_junk(ucfirst($customer['zipcode']));?>"> 
                    
                </div>
              <div class="form-group">
              <p>Street</p>
                    <input type="text" class="form-control input-sm cust_street" name="street" placeholder="Street" value="<?php echo remove_junk(ucfirst($customer['street']));?>"> 
                   
                </div>
                <div class="form-group">
                <p>Barangay</p>
                    <input type="text" class="form-control input-sm cust_brgy" name="brgy" placeholder="Barangay" value="<?php echo remove_junk(ucfirst($customer['brgy']));?>"> 
                   
                </div>
                <div class="form-group">
                <p>City</p>
                    <input type="text" class="form-control input-sm cust_city" name="city" placeholder="City" value="<?php echo remove_junk(ucfirst($customer['city']));?>"> 
                    
                </div>
           </fieldset>
           <button type="button" name="cancel" class="btn btn-default btn-sm pull-left" onclick="goBack();">Cancel</button>
            <button type="submit" name="add_customer" class="btn btn-danger btn-sm btnupdate pull-right" disabled>Apply Changes</button>
            
        </form>
        </div>
      </div>
    </div>
    
   </div>
   <!-- <//?php include_once('layouts/customer_modal.php'); ?> -->
  </div>
  <?php include_once('layouts/footer.php'); ?>
<script>
  $(document).ready(function() {
    //disbaled update button after any changes above inputs
    //
    $(".cust_code").focus( function(){
        $(".btnupdate").prop('disabled',false);
    });
    $(".cust_bizname").focus( function(){
        $(".btnupdate").prop('disabled',false);
    });
    $(".cust_name").focus( function(){
        $(".btnupdate").prop('disabled',false);
    });
    $(".cust_gender").change( function(){
        $(".btnupdate").prop('disabled',false);
    });
    $(".cust_channel").change( function(){
        $(".btnupdate").prop('disabled',false);
    });
    $(".cust_no").focus( function(){
        $(".btnupdate").prop('disabled',false);
    });
    $(".cust_zipcode").focus( function(){
        $(".btnupdate").prop('disabled',false);
    });
    $(".cust_street").focus( function(){
        $(".btnupdate").prop('disabled',false);
    });
    $(".cust_brgy").focus( function(){
        $(".btnupdate").prop('disabled',false);
    });
    $(".cust_city").focus( function(){
        $(".btnupdate").prop('disabled',false);
    });
  });
</script>
