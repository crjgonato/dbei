<?php
$page_title = 'DBEI | Sale Report';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
   page_require_level(3);
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
<div class="row">
<div class="col-md-3 pull-right noti">
<?php echo display_msg($msg); ?>
</div>
</div>
<div class="row">
  <ol class="breadcrumb pull-right">
        <li><a href="admin.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Sales Report</li>
      </ol>
<p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Sales by Date</b></p>
  <div class="col-md-6">
    <div class="panel panel-default">
      
      <div class="panel-body">
          <form class="clearfix" method="post" action="sale_report_process.php">
            <div class="form-group">
              <label class="form-label"></label>
                <div class="input-group">
                  <input type="text" class="datepicker input-sm form-control" name="start-date" autofocus placeholder="From">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-menu-right"></i></span>
                  <input type="text" class="datepicker input-sm form-control" name="end-date" placeholder="To">
                </div>
            </div><br>
            <div class="form-group">
                 <button type="submit" name="submit" class="btn btn-danger btn-sm pull-right">Generate Report</button>
            </div><br>
          </form>
      </div>

    </div>
  </div>

</div>
<?php include_once('layouts/footer.php'); ?>
