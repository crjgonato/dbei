<?php
  $page_title = 'DBEI | Edit product';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
   page_require_level(2);
?>
<?php
$product = find_by_id('products',(int)$_GET['id']);
$all_categories = find_all('categories');
$all_codes = find_all('codes');
$all_units = find_all('unit_measures');
$all_photo = find_all('media');
if(!$product){
  $session->msg("d","Missing product id.");
  redirect('product.php');
}
?>
<?php
 if(isset($_POST['product'])){
    $req_fields = array('product-title','product-categorie','product-quantity','buying-price', 'saleing-price' );
    validate_fields($req_fields);

   if(empty($errors)){
       $p_name  = remove_junk($db->escape($_POST['product-title']));
       $p_cat   = (int)$_POST['product-categorie'];
       $p_code   = (int)$_POST['product-code'];
       $p_unit   = (int)$_POST['product-unit'];
       $p_qty   = remove_junk($db->escape($_POST['product-quantity']));
       $p_buy   = remove_junk($db->escape($_POST['buying-price']));
       $p_sale  = remove_junk($db->escape($_POST['saleing-price']));
       $date = make_date();

       if (is_null($_POST['product-photo']) || $_POST['product-photo'] === "") {
         $media_id = '0';
       } else {
         $media_id = remove_junk($db->escape($_POST['product-photo']));
       }

       $query   = "UPDATE products SET";
       $query  .=" name ='{$p_name}', quantity ='{$p_qty}',";
       $query  .=" buy_price ='{$p_buy}', sale_price ='{$p_sale}', categorie_id ='{$p_cat}',code_id ='{$p_code}',unit_id ='{$p_unit}',media_id='{$media_id}', date_mod='{$date}'";
       $query  .=" WHERE id ='{$product['id']}'";
       $result = $db->query($query);
               if($result && $db->affected_rows() === 1){
                 $session->msg('s',"Product updated succesfully.");
                 redirect('product.php', false);
               } else {
                 $session->msg("d", "Sorry! failed to update.");
                 redirect('edit_product.php?id='.$product['id'], false);
               }

   } else{
       $session->msg("d", $errors);
       redirect('edit_product.php?id='.$product['id'], false);
   }

 }

?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
<div class="row">
<div class="col-md-3 pull-right noti">
<?php echo display_msg($msg); ?>
</div>
</div>
  <div class="row">
  <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Edit Product</b></p>
  <div class="col-md-6">
      <div class="panel panel-default">
        <div class="panel-body">
         <div class="col-md-12">
           <form method="post" action="edit_product.php?id=<?php echo (int)$product['id'] ?>">
              <div class="form-group">
                  <!-- <span class="input-group-addon">
                   <i class="glyphicon glyphicon-th-large"></i>
                  </span> -->
                  <p>Product Name</p>
                  <input type="text" class="form-control initial_input input-sm prdct_name" name="product-title" value="<?php echo remove_junk($product['name']);?>">
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                   <p>Product Category</p>
                    <select class="form-control input-sm prdct_cat" name="product-categorie">
                      <option value=""> Select Category</option>
                      <?php  foreach ($all_categories as $cat): ?>
                        <option value="<?php echo (int)$cat['id']; ?>" <?php if($product['categorie_id'] === $cat['id']): echo "selected"; endif; ?> >
                          <?php echo remove_junk($cat['name']); ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>

                  <div class="col-md-6">
                    <p>Product Code</p>
                      <select class="form-control input-sm" name="product-code">
                      <option value=""> Select Code</option>
                      <?php  foreach ($all_codes as $code): ?>
                        <option value="<?php echo (int)$code['id']; ?>" <?php if($product['code_id'] === $code['id']): echo "selected"; endif; ?> >
                          <?php echo remove_junk($code['name']); ?></option>
                      <?php endforeach; ?>
                    </select> 
                 </div>
                </div>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <p>Product Quantity</p>
                    <input type="number" class="form-control input-sm" name="product-quantity" value="<?php echo remove_junk($product['quantity']); ?>" readonly style="cursor:not-allowed;">
                    </div>
                  <div class="col-md-6">
                   <p>Unit of Measurements</p>
                    <select class="form-control prdct_unit input-sm" name="product-unit">
                      <option value=""> Select a unit</option>
                      <?php  foreach ($all_units as $unit): ?>
                        <option value="<?php echo (int)$unit['id']; ?>" <?php if($product['unit_id'] === $unit['id']): echo "selected"; endif; ?> >
                          <?php echo remove_junk($unit['name']); ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <p>Buying Price</p>
                      <div class="input-group">
                        <span class="input-group-addon">
                        P
                        </span>
                        <input type="text" class="form-control input-sm prdct_buy" name="buying-price" value="<?php echo remove_junk($product['buy_price']);?>">
                      </div>
                    </div>
                  <div class="col-md-6">
                   
                     <p>Selling Price</p>
                     <div class="input-group">
                       <span class="input-group-addon">
                         P
                       </span>
                       <input type="text" class="form-control input-sm prdct_sell" name="saleing-price" value="<?php echo remove_junk($product['sale_price']);?>">
                       <!-- <span class="input-group-addon">.00</span> -->
                      
                   </div>
                  </div>
               </div>
              </div>
             
              <button type="button" name="cancel" class="btn btn-default btn-sm pull-left" onclick="goBack();">Cancel</button>
              <button type="submit" name="product" class="btn btn-danger btn-sm btnupdate pull-right" disabled>Apply changes</button>&nbsp;&nbsp;&nbsp;
              
             
          </form>
         </div>
        </div>
      </div>
  </div>
  </div>

<?php include_once('layouts/footer.php'); ?>
<script>
  $(document).ready(function() {
    //disbaled update button after any changes above inputs
    //
    $(".prdct_name").focus( function(){
        $(".btnupdate").prop('disabled',false);
    });

    $(".prdct_cat").change( function(){
      $(".btnupdate").prop('disabled',false);
    });

    $(".prdct_code").change( function(){
      $(".btnupdate").prop('disabled',false);
    });

    $(".prdct_unit").change( function(){
      $(".btnupdate").prop('disabled',false);
    });

    $(".prdct_buy").focus( function(){
      $(".btnupdate").prop('disabled',false);
    });
    $(".prdct_sell").focus( function(){
      $(".btnupdate").prop('disabled',false);
    });
  });
</script>

