<?php
  if(isset($_POST['add_sale'])){
    $req_fields = array('s_id','sales_invoice','customer','quantity','price','total', 'date' );
    validate_fields($req_fields);
        if(empty($errors)){
          $p_id      = $db->escape((int)$_POST['s_id']);
          $s_si      = $db->escape($_POST['sales_invoice']);
          $s_customer = $db->escape($_POST['customer']);
          $s_qty     = $db->escape($_POST['quantity']);
          $s_total   = $db->escape($_POST['total']);
          $s_term = $db->escape($_POST['term']);
          $date      = $db->escape($_POST['date']);
          $s_date    = make_date();

          $sql  = "INSERT INTO sales (";
          $sql .= " product_id,sales_invoice,customer,qty,price,term,date";
          $sql .= ") VALUES (";
          $sql .= "'{$p_id}','{$s_si}','{$s_customer}','{$s_qty}','{$s_total}','{$s_term}','{$s_date}'";
          $sql .= ")";

                if($db->query($sql)){
                  update_product_qty($s_qty,$p_id);
                  $session->msg('s',"Sale successfully.");
                  redirect('sales.php', false);
                } else {
                  $session->msg("d", "Sorry! failed to create a sale.");
                  redirect('add_sale.php', false);
                }
        } else {
           $session->msg("d", $errors);
           redirect('add_sale.php',false);
        }
  }

?>

