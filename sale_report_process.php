<?php
$page_title = 'DBEI | Sales Report';
$results = '';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
   page_require_level(3);
?>
<?php
  if(isset($_POST['submit'])){
    $req_dates = array('start-date','end-date');
    validate_fields($req_dates);

    if(empty($errors)):
      $start_date   = remove_junk($db->escape($_POST['start-date']));
      $end_date     = remove_junk($db->escape($_POST['end-date']));
      $results      = find_sale_by_dates($start_date,$end_date);
    else:
      $session->msg("d", $errors);
      redirect('sales_report.php', false);
    endif;

  } else {
    $session->msg("d", "Select dates");
    redirect('sales_report.php', false);
  }
?>
<!doctype html>
<html>
<?php include_once('layouts/header_reports.php'); ?>
<body>
  <?php if($results): ?>
    <div class="page-break">
       <div class="sale-head">
           <h3>Sales Report</h3>
           <strong><?php if(isset($start_date)){ echo $start_date;}?> To <?php if(isset($end_date)){echo $end_date;}?> </strong>
           
            &nbsp;&nbsp;&nbsp;<input type="button" id="printsales" onclick="myFunction()" name="" class="btn btn-danger btn-sm" value="Print">
            <input type="button" id="cancelprintsales" onclick="goBack()" name="" class="btn btn-default btn-sm pull-right" value="Cancel">
       </div><br>
      <table class="table table-border">
        <thead>
          <tr>
              <th style="width: 10%;">Date</th>
              <th>Product Title</th>
              <th>Buying Price</th>
              <th>Selling Price</th>
              <th>Total Qty</th>
              <th>TOTAL</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($results as $result): ?>
           <tr>
              <td><?php echo remove_junk($result['date']);?></td>
              <td class="desc"><h6><?php echo remove_junk(ucfirst($result['name']));?></h6></td>
              <td class=""><?php echo remove_junk($result['buy_price']);?></td>
              <td class=""><?php echo remove_junk($result['sale_price']);?></td>
              <td class=""><?php echo remove_junk($result['total_sales']);?></td>
              <td class=""><?php echo remove_junk($result['total_saleing_price']);?></td>
          </tr>
        <?php endforeach; ?>
        </tbody>
        <tfoot>
         <tr class="text-right">
           <td colspan="4"></td>
           <td colspan="1">Grand Total</td>
           <td> P
           <?php echo number_format(total_price($results)[0], 2);?>
          </td>
         </tr>
         <tr class="text-right">
           <td colspan="4"></td>
           <td colspan="1">Profit</td>
           <td> P<?php echo number_format(total_price($results)[1], 2);?></td>
         </tr>
        </tfoot>
      </table>
    </div>
  <?php
    else:
        $session->msg("d", "Sorry no sales has been found. ");
        redirect('sales_report.php', false);
     endif;
  ?>
</body>
</html>
<?php if(isset($db)) { $db->db_disconnect(); } ?>
<script>

function myFunction() {
  var printButton = document.getElementById("printsales");
  var cancelButton = document.getElementById("cancelprintsales");
    printButton.style.visibility = 'hidden';
    cancelButton.style.visibility = 'hidden';
    window.print();
    printButton.style.visibility = 'visible';
    cancelButton.style.visibility = 'visible';
}
</script>