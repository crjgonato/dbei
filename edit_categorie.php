<?php
  $page_title = 'DBEI | Edit categorie';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
?>
<?php
  //Display all catgories.
  $categorie = find_by_id('categories',(int)$_GET['id']);
  if(!$categorie){
    $session->msg("d","Missing categorie id.");
    redirect('categorie.php');
  }
?>

<?php
if(isset($_POST['edit_cat'])){
  $req_field = array('categorie-name');
  validate_fields($req_field);
  $cat_name = remove_junk($db->escape($_POST['categorie-name']));
  $date = make_date();
  if(empty($errors)){
        $sql = "UPDATE categories SET name='{$cat_name}', date_mod='{$date}'";
       $sql .= " WHERE id='{$categorie['id']}'";
     $result = $db->query($sql);
     if($result && $db->affected_rows() === 1) {
       $session->msg("s", "Category updated succesfully.");
       redirect('categorie.php',false);
     } else {
       $session->msg("d", "Sorry! failed to update.");
       redirect('categorie.php',false);
     }
  } else {
    $session->msg("d", $errors);
    redirect('categorie.php',false);
  }
}
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
<div class="row">
<div class="col-md-3 pull-right noti">
<?php echo display_msg($msg); ?>
</div><br>
<p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Edit Category</b></p>
   <div class="col-md-3">
     <div class="panel panel-default">
       <div class="panel-body">
         <form method="post" action="edit_categorie.php?id=<?php echo (int)$categorie['id'];?>">
           <div class="form-group">
           <p>Category name</p>
               <input type="text" class="form-control input-sm initial_input cat" name="categorie-name" value="<?php echo remove_junk(ucfirst($categorie['name']));?>">
           </div>
           <button type="button" name="cancel" class="btn btn-default btn-sm pull-left" onclick="goBack();">Cancel</button>
           <button type="submit" name="edit_cat" class="btn btn-danger btn-sm btnupdate pull-right" disabled>Apply Changes</button>
           
       </form>
       </div>
     </div>
   </div>
</div>



<?php include_once('layouts/footer.php'); ?>
<script>
  $().ready(function() {
    //disbaled update button after any changes above inputs
    //
    $(".cat").focus( function(){
        $(".btnupdate").prop('disabled',false);
    });
  });
</script>
