<?php
  $page_title = 'DBEI | Dashboard';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
   page_require_level(1);
?>
<?php
    $c_categorie     = count_by_id('categories');
    $c_product       = count_by_id('products');
    $c_sale          = count_by_id('sales');
    $c_user          = count_by_id('users');
    $c_units         = count_by_id('unit_measures');
    $c_codes         = count_by_id('codes');
    $c_customers     = count_by_id('customers');
    $c_selesman      = count_by_id('salesman');
    $products_sold   = find_higest_saleing_product('13');
    $recent_products = find_recent_product_added('13');
    $recent_sales    = find_recent_sale_added('13');
    $products_avail  = find_products_avail();
    $route_coverage  = join_route_table();
    $city_sold       = find_highest_sales_by_city();
    $salesbydates    = find_sales_date();
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="20;" />  
<div class="row">
  <div class="col-md-3 pull-right noti">
    <?php echo display_msg($msg); ?>
  </div>
</div>
<p class="text-muted pull-right"> Dashboard</p>
<p class="text-muted">REAL-TIME REPORTS&nbsp;</p>


<div class="row">
  <!-- <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;Transaction Panels</p> -->
  <div class="col-md-3">
    <div class="panel panel-box clearfix">
      <div class="panel-icon pull-left bg-yellow">
        <i class="glyphicon glyphicon-tasks"></i>
      </div>
      <div class="panel-value pull-right" >
        <h4 class="margin-top text-muted"> <?php echo $c_sale['total']; ?></h4>
        <p class="text-muted"><b>Sales</b></p>
          <!-- <h6 class="text-muted pull-left" onclick="location.href = './sales.php';" style="cursor: pointer;color:rgba(0, 0, 0, 0.34);">View</h6> -->
      </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="panel panel-box clearfix">
      <div class="panel-icon pull-left bg-blue">
        <i class="glyphicon glyphicon-shopping-cart"></i>
      </div>
      <div class="panel-value pull-right">
        <h4 class="margin-top text-muted">0 </h4>
        <p class="text-muted"><b>Purchases</b></p>
          <!-- <h6 class="text-muted pull-left" onclick="location.href = './purchases.php';"  style="cursor: pointer;color:rgba(0, 0, 0, 0.34);">View</h6> -->
      </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="panel panel-box clearfix">
      <div class="panel-icon pull-left bg-blue">
        <i class="glyphicon glyphicon-stats"></i>
      </div>
      <div class="panel-value pull-right" >
        <h4 class="margin-top text-muted"> <?php  echo $c_product['total']; ?> </h4>
        <p class="text-muted"><b>Products</b></p>
          <!-- <h6 class="text-muted pull-left"  onclick="location.href = './product.php';" style="cursor: pointer;color:rgba(0, 0, 0, 0.34);">View</h6>  -->
      </div>
     </div>
  </div>

  <div class="col-md-3">
    <div class="panel panel-box clearfix">
      <div class="panel-icon pull-left bg-yellow">
        <i class="glyphicon glyphicon-briefcase"></i>
      </div>
      <div class="panel-value pull-right">
        <h4 class="margin-top text-muted"> <?php  echo $c_selesman['total']; ?></h4>
        <p class="text-muted"><b>Salesman</b></p>
        <!-- <h6 class="text-muted pull-left"  onclick="location.href = './salesman.php';"  style="cursor: pointer;color:rgba(0, 0, 0, 0.34);">View</h6> -->
      </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="panel panel-box clearfix">
      <div class="panel-icon pull-left bg-green">
        <i class="glyphicon glyphicon-user"></i>
      </div>
      <div class="panel-value pull-right">
        <h4 class="margin-top text-muted"> <?php echo $c_user['total']; ?> </h4>
        <p class="text-muted"><b>Users</b></p>
           <!-- <h6 class="text-muted pull-left" onclick="location.href = './users.php';" style="cursor: pointer;color:rgba(0, 0, 0, 0.34);">View</h6>  -->
      </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="panel panel-box clearfix">
      <div class="panel-icon pull-left bg-blue">
        <i class="glyphicon glyphicon-credit-card"></i>
      </div>
      <div class="panel-value pull-right" >
        <h4 class="margin-top text-muted"><?php echo $c_customers['total']; ?> </h4>
        <p class="text-muted"><b>Customers</b></p>
          <!-- <h6 class="text-muted pull-left" onclick="location.href = './customers.php';" style="cursor: pointer;color:rgba(0, 0, 0, 0.34);">View</h6>  -->
      </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="panel panel-box clearfix">
      <div class="panel-icon pull-left bg-yellow">
        <i class="glyphicon glyphicon-repeat"></i>
      </div>
      <div class="panel-value pull-right">
        <h4 class="margin-top text-muted"> 0</h4>
        <p class="text-muted"><b>Returns</b></p>
           <!-- <h6 class="text-muted pull-left"  onclick="#"  style="cursor: pointer;color:rgba(0, 0, 0, 0.34);">View</h6> -->
      </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="panel panel-box clearfix">
      <div class="panel-icon pull-left bg-yellow">
        <i class="glyphicon glyphicon-download"></i>
      </div>
      <div class="panel-value pull-right">
        <h4 class="margin-top text-muted"> 0</h4>
        <p class="text-muted"><b>Receivables</b></p>
          <!-- <h6 class="text-muted pull-left"  onclick="#"  style="cursor: pointer;color:rgba(0, 0, 0, 0.34);">View</h6> -->
      </div>
    </div>
  </div>
</div>
<div class="row">
    <div class="col-md-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          <strong><span>Sales per Date</span></strong>
        </div>
        <div class="dashboard_salesbycity">
          <div class="panel-body">
            <table class="table table-bordered table-condensed">
              <thead>
                <tr>
                  <!-- <th>#</th> -->
                  <th>Dates</th> 
                  <th>Total Sales</th>
                <tr>
              </thead>
              <tbody>
                <?php foreach ($salesbydates as $salesbydate): ?>
                  <tr>
                    <!-- <td><//?php echo count_id();?></td> -->
                    <td><h6><a><?php echo remove_junk(read_datenotime($salesbydate['date'])); ?></a></h6></td>
                   
                    <td><h6><a><?php echo remove_junk(first_character($salesbydate['totalSolds'])); ?></a></h6></td>
                    <!-- <td><h6><a><//?php echo remove_junk(first_character($salesbydate['totalQty'])); ?></a></h6></td> -->
                  </tr>
                <?php endforeach; ?>
              <tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

   <div class="col-md-6">
     <div class="panel panel-default">
        <div class="panel-heading">
          <strong><span>Sales per Customer</span></strong>
        </div>
        <div class="dashboard_salesbycity">
          <div class="panel-body">
            <table class="table table-bordered table-condensed">
              <thead>
                <tr>
                  <!-- <th>#</th> -->
                  <th>Name</th>
                  <th>Cities</th> 
                  <!-- <th>Total Sold</th>   -->
                  <th>Total Quantity</th>          
                <tr>
              </thead>
              <tbody>
                <?php foreach ($city_sold as $city_sold): ?>
                  <tr>
                    <!-- <td><//?php echo count_id();?></td> -->
                    <td><h6><a><?php echo remove_junk(first_character($city_sold['name'])); ?></a></h6></td>
                    <td><h6><a><?php echo remove_junk(first_character($city_sold['city'])); ?></a></h6></td>
                    <!-- <td><h6><a><//?php echo remove_junk(first_character($city_sold['totalSold'])); ?></a></h6></td> -->
                    <td><h6><a><?php echo remove_junk(first_character($city_sold['totalQty'])); ?></a></h6></td>
                  </tr>
                <?php endforeach; ?>
              <tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>



  <div class="row">
    <div class="col-md-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          <strong><span>Stocks Availability</span></strong>
        </div>
        <div class="dashboard_products_avail">
          <div class="panel-body">
          <!-- <h5 class="text-muted">Working on it..</h5> -->
          <table class="table table-bordered table-condensed">
              <thead>
                <tr>
                  <th>Products</th>
                  <th>Quantity</th>
                  <!-- <th>Actions</th> -->
                <tr>
              </thead>
              <tbody>
                <?php foreach ($products_avail as  $products_avail): ?>
                <tr>
                  <td><h6><a><?php echo remove_junk(first_character($products_avail['name'])); ?></a></h6></td>
                  <td><h6><a class=" quantity"><?php echo remove_junk($products_avail['quantity']); ?></a><h6></td>
                  <!-- <td><h6><a href="">Purchase</a><h6></td> -->
                </tr>
                <?php endforeach; ?>
              <tbody>
            </table>
        </div>
        </div>
      </div>
    </div>

   <div class="col-md-6">
     <div class="panel panel-default">
       <div class="panel-heading">
         <strong><span>Route Coverage</span></strong>
        </div>
        <div class="dashboard_products_avail">
          <div class="panel-body">
          <!-- <h5 class="text-muted">Working on it..</h5> -->
          <table class="table table-bordered table-condensed">
              <thead>
                <tr>
                  <th>Salesman</th>
                  <th class="text-center">Status</th>
                  <th>Coverage</th>
                  <!-- <th>Actions</th> -->
                <tr>
              </thead>
              <tbody>
                <?php foreach ($route_coverage as  $route_coverage): ?>
                <tr>
                  <td><h6><a><?php echo remove_junk(first_character($route_coverage['salesman'])); ?></a></h6></td>
                   <td class="text-center">
                      <?php if($route_coverage['status_id'] === '1'): ?>
                        <span class="label label-success"><?php echo "On-Field"; ?></span>
                        
                      <?php elseif($route_coverage['status_id'] === '2'): ?>
                        <span class="label label-primary"><?php echo "Standby"; ?></span>

                      <?php elseif($route_coverage['status_id'] === '3'): ?>
                        <span class="label label-default"><?php echo "Cleared"; ?></span>

                      <?php endif;?>
                  </td>
                  <td><h6><a><?php echo remove_junk($route_coverage['coverage']); ?></a><h6></td>
                  <!-- <td><h6><a href="">Purchase</a><h6></td> -->
                </tr>
                <?php endforeach; ?>
              <tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <!-- <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;What products are the most?</p> -->
    <div class="col-md-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          <strong>
            <!-- <span class="glyphicon glyphicon-th"></span> -->
            <span>Highest Selling Products</span>
          </strong>
        </div>
        <div class="dashboard_products">
          <div class="panel-body">
            <table class="table table-bordered table-condensed">
              <thead>
                <tr>
                  <th>Products</th>
                  <th>Sold</th>
                  <th>Quantity</th>
                <tr>
              </thead>
              <tbody>
                <?php foreach ($products_sold as  $product_sold): ?>
                <tr>
                  <td><h6><a><?php echo remove_junk(first_character($product_sold['name'])); ?></a></h6></td>
                  <td><h6><a><?php echo remove_junk($product_sold['totalSold']); ?></a><h6></td>
                  <td><h6><a><?php echo remove_junk($product_sold['totalQty']); ?></a><h6></td>
                </tr>
                <?php endforeach; ?>
              <tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
          <div class="col-md-4">
            <div class="panel panel-default">
              <div class="panel-heading">
                <strong>
                  <!-- <span class="glyphicon glyphicon-th"></span> -->
                  <span>Latest Sales</span>
                </strong>
              </div>
              <div class="dashboard_products">
                <div class="panel-body">
                  <table class="table table-bordered table-condensed">
                      <thead>
                        <tr>
                          <!-- <th class="text-center" style="width: 50px;">#</th> -->
                          <th>Product Name</th>
                          <!-- <th>Date</th> -->
                          <th> Sale</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($recent_sales as  $recent_sale): ?>
                          <tr>
                            <!-- <td class="text-center"><//?php echo count_id();?></td> -->
                            <td>
                              <h6>
                                <a href="view_sale.php?id=<?php echo (int)$recent_sale['id']; ?>">
                                <?php echo remove_junk(first_character($recent_sale['name'])); ?>
                                </a>
                              </h6>
                            </td>
                            <!-- <td><//?php echo remove_junk(ucfirst($recent_sale['date'])); ?></td> -->
                            <td>
                              <h6>
                                <a>
                                  P <?php echo remove_junk(first_character($recent_sale['price'])); ?>
                                </a>
                              </h6>
                            </td>
                          </tr>
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

    <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-heading">
              <strong>
                <!-- <span class="glyphicon glyphicon-th"></span> -->
                <span>Recently Added Products</span>
              </strong>
            </div>
            <div class="dashboard_products">
          <div class="panel-body">
            <table class="table table-bordered table-condensed">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Price</th>       
                <tr>
              </thead>
              <tbody>
                <?php foreach ($recent_products as $recent_product): ?>
                <tr>
                  <!-- <td><//?php echo count_id();?></td> -->
                  <td>
                    <h6>
                      <a href="view_product.php?id=<?php echo (int)$recent_product['id'];?>"><?php echo remove_junk(first_character($recent_product['name'])); ?></a>
                    </h6>
                  </td>                 
                  <td>
                    <h6>
                      <a>P <?php echo remove_junk(first_character($recent_product['sale_price'])); ?></a>
                    </h6>
                  </td>               
                </tr>
                <?php endforeach; ?>
              <tbody>
            </table>
          </div>
        </div>
            <!-- <div class="dashboard_products">
                <div class="panel-body">
                  <div class="list-group">
                      <//?php foreach ($recent_products as  $recent_product): ?>
                            <a class="list-group-item clearfix" href="view_product.php?id=<//?php echo    (int)$recent_product['id'];?>">
                                <h6 class="list-group-item-heading">
                                <//?php if($recent_product['media_id'] === '0'): ?>
                                    <img class="img-avatar img-circle" src="uploads/products/no_image.jpg" alt="">
                                  <//?php else: ?>
                                  <img class="img-avatar img-circle" src="uploads/products/<//?//php echo $recent_product['image'];?>" alt="" />
                                <//?php endif;?>
                                <//?php echo remove_junk(first_character($recent_product['name']));?>
                                  <span class="pull-right"><b>
                                P <//?php echo remove_junk($recent_product['sale_price']); ?>
                                  </b></span>
                                </h6>
                                <span class="list-group-item-text pull-right">
                                <//?php echo remove_junk(first_character($recent_product['categorie'])); ?>
                              </span>
                          </a>
                      <//?php endforeach; ?>
                  </div>
                </div>
              </div> -->
        </div>
    </div>
</div>
<?php include_once('layouts/footer.php'); ?>
 <script>
  //console.log(document.cookie);
    $(document).ready(function() {
      // the following will select all 'td' elements with class "of_number_to_be_evaluated"
      // if the TD element has a '-', it will assign a 'red' class, and do the same for green.
      $("table .quantity:contains('-')").addClass('red');
    });
  </script>


