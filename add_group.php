<?php
  $page_title = 'DBEI | Add Group';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
   page_require_level(1);
?>
<?php
  if(isset($_POST['add'])){

   $req_fields = array('group-name','group-level');
   validate_fields($req_fields);

  //  if(find_by_groupName($_POST['group-name']) === false ){
  //    $session->msg('d','<b>Sorry!</b> Entered Group Name already in database!');
  //    redirect('add_group.php', false);
  //  }elseif(find_by_groupLevel($_POST['group-level']) === false) {
  //    $session->msg('d','<b>Sorry!</b> Entered Group Level already in database!');
  //    redirect('add_group.php', false);
  //  }
   if(empty($errors)){
        $name = remove_junk($db->escape($_POST['group-name']));
        $level = remove_junk($db->escape($_POST['group-level']));
        $status = remove_junk($db->escape($_POST['status']));
        $date = make_date();

        $query  = "INSERT INTO user_groups (";
        $query .="group_name,group_level,group_status,date";
        $query .=") VALUES (";
        $query .=" '{$name}', '{$level}','{$status}','{$date}'";
        $query .=")";
        if($db->query($query)){
          //sucess
          $session->msg('s',"Group created succesfully. ");
          redirect('group.php', false);
        } else {
          //failed
          $session->msg("d", "Sorry! failed to create a group.");
          redirect('add_group.php', false);
        }
   } else {
     $session->msg("d", $errors);
      redirect('add_group.php',false);
   }
 }
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
<div class="row"><br>
<p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>New Group</b></p>
 <div class="col-md-7">
   
    <!-- <div class="text-center">
       <h3>Add new user Group</h3>
     </div> -->

     <div class="col-md-3 pull-right noti">
        <?php echo display_msg($msg); ?>
    </div>
    <div class="col-md-8">
     <div class="panel panel-default">
       <div class="panel-heading">
    <div class="panel-body">
      <div class="col-md-9">
        <form method="post" action="add_group.php" class="clearfix">
        <div class="form-group">
              <label for="name" class="control-label">Group Name</label>
              <input type="name" class="form-control input-sm initial_input" autofocus name="group-name">
        </div>
        <div class="form-group">
              <label for="level" class="control-label">Group Level</label>
              <input type="number" class="form-control input-sm" name="group-level">
        </div>
        <div class="form-group">
          <label for="status">Status</label>
            <select class="form-control input-sm" name="status">
              <option value="1">Active</option>
              <option value="0">Deactive</option>
            </select>
        </div>
        <div class="form-group clearfix">
          <button type="button" name="cancel" class="btn btn-default btn-sm pull-left" onclick="goBack();">Cancel</button>
          <button type="submit" name="add" class="btn btn-danger btn-sm pull-right">Add Group</button>
                
        </div>
      </form>
</div>


</div>
</div>
</div>
</div>
</div>
</div>

<?php include_once('layouts/footer.php'); ?>
