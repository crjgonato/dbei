<?php
  $page_title = 'DBEI | Edit Account';
  require_once('includes/load.php');
   page_require_level(3);
?>
<?php
//update user image
  if(isset($_POST['submit'])) {
  $photo = new Media();
  $user_id = (int)$_POST['user_id'];
  $photo->upload($_FILES['file_upload']);
  if($photo->process_user($user_id)){
    $session->msg('s','photo has been uploaded.');
    redirect('edit_account.php');
    } else{
      $session->msg('d',join($photo->errors));
      redirect('edit_account.php');
    }
  }
?>
<?php
 //update user other info
  if(isset($_POST['update'])){
    $req_fields = array('name','username' );
    validate_fields($req_fields);
    if(empty($errors)){
             $id = (int)$_SESSION['user_id'];
           $name = remove_junk($db->escape($_POST['name']));
       $username = remove_junk($db->escape($_POST['username']));
            $sql = "UPDATE users SET name ='{$name}', username ='{$username}' WHERE id='{$id}'";
    $result = $db->query($sql);
          if($result && $db->affected_rows() === 1){
            $session->msg('s',"Acount updated succesfully.");
            redirect('edit_account.php', false);
          } else {
            $session->msg("d", "Sorry! failed to update.");
            redirect('edit_account.php', false);
          }
    } else {
      $session->msg("d", $errors);
      redirect('edit_account.php',false);
    }
  }
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
<div class="row"><br>
  <div class="col-md-3 pull-right noti">
    <?php echo display_msg($msg); ?>
  </div>
<div class="row">
 <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Settings</b></p>
  <div class="col-md-6 hidden">
      <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-heading clearfix">
            <!-- <span class="glyphicon glyphicon-camera"></span> -->
            <span>Profile photo</span>
          </div>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-4">
                <img class="img-circle img-size-2" src="uploads/users/<?php echo $user['image'];?>" alt="">
            </div>
            <div class="col-md-8">
              <form class="form" action="edit_account.php" method="POST" enctype="multipart/form-data">
              <div class="form-group">
                <input type="file" name="file_upload" multiple="multiple" class="btn btn-file btn-default"/>
              </div>
              <div class="form-group">
                <input type="hidden" name="user_id" value="<?php echo $user['id'];?>">
                 <button type="submit" name="submit" class="btn btn-danger" disabled>Save photo</button>
              </div>
             </form>
            </div>
          </div>
        </div>
      </div>
  </div>
  
  <div class="col-md-2">
    <div class="panel default">
         <ul class="nav nav-stacked">
              <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Account</a></li>
              <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Tab 2</a></li>
              <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Tab 3</a></li>
              <li class=""><a href="change_password.php" >Change Password</a></li>
            </ul>
    </div>
  </div>

  <div class="col-md-10 bg-blue">
          <!-- Tabs -->
          <div class="nav panel-default">
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                 <div class="panel-default">
                        <div class="panel-heading clearfix">
                          <!-- <span class="glyphicon glyphicon-edit"></span> -->
                          <!-- <span>Edit Account</span> -->
                          <!-- <a href="change_password.php" title="change password" class="btn btn-default pull-right" >Change Password</a> -->
                        </div>
                        <div class="col-md-6">
                            <div class="panel-body">
                            <form method="post" action="edit_account.php?id=<?php echo (int)$user['id'];?>" class="clearfix">
                              <div class="form-group">
                                    <label for="name" class="control-label">Name</label>
                                    <input type="name" class="form-control name" name="name" value="<?php echo remove_junk(ucwords($user['name'])); ?>">
                              </div>
                              <div class="form-group">
                                    <label for="username" class="control-label">Username</label>
                                    <input type="text" class="form-control username" name="username" value="<?php echo remove_junk(ucwords($user['username'])); ?>">
                              </div>
                              <div class="form-group clearfix">
                                    
                                      <button type="submit" name="update" class="btn btn-danger btnupdate" disabled>Update</button>
                              </div>
                            </form>
                        </div>
                        </div>
                      </div>
              </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                 
                </div>

                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_3">
                  
                </div>

                <!-- /.tab-pane -->
              </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
</div>
</div>


<?php include_once('layouts/footer.php'); ?>
<script>
  $().ready(function() {
    
    //disbaled update button after any changes above inputs
    //
    $(".name").change( function(){
        $(".btnupdate").prop('disabled',false);
    });
    $(".username").change( function(){
        $(".btnupdate").prop('disabled',false);
    });

  });
</script>
