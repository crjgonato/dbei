<?php
  $page_title = 'DBEI | Routes';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
  
  //$all_customers = find_all('customers');
  $all_salesman = find_salesman();
  $all_employees = find_all('employees');
  $all_status = find_all('status');
  $routes = join_route_table();
  $route_history = join_route_history_table();
  $all_units = find_all('unit_measures');
?>
<?php
 if(isset($_POST['add_route'])){
  //  $req_field = array('name','gender','balance','date_paid','contact','address');
  $req_field = array('salesman','coverage');
   validate_fields($req_field);
        $salesman = remove_junk($db->escape($_POST['salesman']));
        $coverage = remove_junk($db->escape($_POST['coverage']));
        $load = remove_junk($db->escape($_POST['load']));
        $unit = remove_junk($db->escape($_POST['unit']));
        $date = remove_junk($db->escape($_POST['date']));
        $status = remove_junk($db->escape($_POST['status']));
        $approver = remove_junk($db->escape($_POST['approver']));

   if(empty($errors)){
        $sql  = "INSERT INTO routes (";
        $sql .="salesman_id,coverage,loads,unit_id,date,status,approver";
        $sql .=") VALUES (";
        $sql .="'{$salesman}','{$coverage}','{$load}','{$unit}','{$date}','{$status}','{$approver}'";
        $sql .=")";
        
      if($db->query($sql)){
        $session->msg("s", "Routes created successfully.");
        redirect('route.php',false);
      } else {
        $session->msg("d", "Sorry! failed to create.");
        redirect('route.php',false);
      }
   } else {
     $session->msg("d", $errors);
     redirect('route.php',false);
   }
 }
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
  <div class="row">
  <div class="col-md-3 pull-right noti">
  <?php echo display_msg($msg); ?>
</div>
  </div>
   <div class="row">
     <ol class="breadcrumb pull-right">
        <li><a href="admin.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Routes</li>
      </ol>
   <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Routes</b></p>
    <div class="col-md-2 hidden">
      <div class="panel panel-default">
        <div class="panel-body">
          <form method="post" action="route.php">
           <div class="form-group ">
                <select class="form-control input-sm" name="salesman">
                        <option disabled selected>Select employee</option>
                        <?php  foreach ($all_employees as $employees): ?>
                          <option value="<?php echo $employees['id'] ?>">
                            <?php echo $employees['name'] ?></option>
                        <?php endforeach; ?>    
                    </select>             
           </div>
           <div class="form-group">
              <input type="text" class="form-control input-sm" name="load" placeholder="Quantity">
           </div>
            <div class="form-group">
                <!-- <input type="text" class="form-control" name="contact" placeholder="Coverage" autofocus require="required">  -->
                   <textarea class="form-control input-sm" name="coverage" placeholder="Coverage" require="required"></textarea>
            </div>
            <div class="form-group">
              <select class="form-control input-sm" name="unit">
                        <option disabled selected>Select units</option>
                        <?php  foreach ($all_units as $all_unit): ?>
                          <option value="<?php echo $all_unit['id'] ?>">
                            <?php echo $all_unit['name'] ?></option>
                        <?php endforeach; ?>    
                    </select>  

            </div>
            <div class="form-group">
                <input type="text" class="form-control input-sm datepicker" name="date" placeholder="Date" require="required"> 
            </div>
            <div class="form-group">
                <!-- <input type="text" class="form-control input-sm datepicker" name="date" placeholder="Date" require="required">  -->
                <select class="form-control input-sm" name="status">
                  <!-- <option>Select status</option>
                  <option value="1">On-Field</option>
                  <option value="2">Cleared</option>
                  <option value="3">Standby</option> -->
                  <option disabled selected>Select status</option>
                        <?php  foreach ($all_status as $status): ?>
                          <option value="<?php echo $status['id'] ?>">
                            <?php echo $status['name'] ?></option>
                        <?php endforeach; ?> 
                </select>
            </div>
            <div class="form-group ">
                <select class="form-control input-sm" name="approver">
                        <option disabled selected>Select approver</option>
                        <?php  foreach ($all_employees as $employees): ?>
                          <option value="<?php echo $employees['name'] ?>">
                            <?php echo $employees['name'] ?></option>
                        <?php endforeach; ?>    
                    </select>             
           </div>
           
            <button type="submit" name="add_route" class="btn btn-sm btn-danger pull-right">Add Route</button>
        </form>
        </div>
      </div>
    </div>
    
    
    <div class="col-md-12">
      <div class="panel panel-default">
          <div class="panel-body">
            <p class="pull-left text-muted"><b>Current Routes</b></p>
          <div class="col-sm-4 pull-right input-group">
              <input type="text" class="form-control input-sm" id="search2" placeholder="Search route here..">
                <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
            </div>
            <table class="table table-bordered table-condensed">
              <thead>
                  <tr>
                      <th class="text-center" style="width: 50px;">#</th>
                      <th>Dates</th>
                      <th>Salesman</th>
                      <th>Coverage</th>
                      <th>Unit</th>
                      <th>Quantity</th>
                      <th>Returns</th>
                      
                      <th class="text-center">Status</th>
                      <th class="text-center">Approver</th>
                      <th class="text-center" colspan="2">Actions</th>
                    
                  </tr>
              </thead>
              <tbody class="tablesearch2">
                <?php foreach ($routes as $route):?>
                <tr>
                    <td class="text-center"><?php echo count_id();?></td>
                    <td><?php echo remove_junk(ucfirst($route['date'])); ?></td>
                    <td><b><?php echo remove_junk(ucfirst($route['salesman'])); ?></b></td>
                    <td><?php echo remove_junk(ucfirst($route['coverage'])); ?></td>
                    <td><?php echo remove_junk(ucfirst($route['units'])); ?></td>
                    <td><?php echo remove_junk(ucfirst($route['loads'])); ?></td>
                    <td><?php echo remove_junk(ucfirst($route['returns'])); ?></td>
                    <td class="text-center">
                        <?php if($route['status_id'] === '1'): ?>
                          <span class="label label-success"><?php echo "On-Field"; ?></span>
                          
                        <?php elseif($route['status_id'] === '2'): ?>
                          <span class="label label-primary"><?php echo "Standby"; ?></span>

                        <?php elseif($route['status_id'] === '3'): ?>
                          <span class="label label-default"><?php echo "Cleared"; ?></span>

                        <?php endif;?>
                    </td>
                    <td><?php echo remove_junk(ucfirst($route['approver'])); ?></td>
                    <td class="text-center">
                      <div class="btn-group">
                        <a href="edit_route.php?id=<?php echo (int)$route['id'];?>" title="Edit" data-toggle="tooltip">
                        &nbsp;&nbsp;<i class="glyphicon glyphicon-edit"></i>&nbsp;&nbsp;
                      </a>
                      </div>
                    </td>
                    <!-- <td class="text-center">
                      <div class="btn-group">
                        <a href="#.php?id=<//?php echo (int)$route['id'];?>" title="Returns" data-toggle="tooltip">
                        &nbsp;&nbsp;<i class="glyphicon glyphicon glyphicon-retweet"></i>&nbsp;&nbsp;
                      </a>
                      </div>
                    </td> -->
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="panel panel-default">
          <div class="panel-body">
            <p class="pull-left text-muted"><b>Past Routes</b></p>
          <div class="col-sm-4 pull-right input-group">
              <input type="text" class="form-control input-sm" id="search1" placeholder="Search route history here..">
                <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
            </div>
            <table class="table table-bordered table-condensed">
              <thead>
                  <tr>
                      <th class="text-center" style="width: 50px;">#</th>
                      <th>Dates</th>
                      <th>Salesman</th>
                      <th>Coverage</th>
                      <th>Unit</th>
                      <th>Quantity</th>
                      <th>Returns</th>
                      
                      <th class="text-center">Status</th>
                      <th class="text-center">Approver</th>
                      <!-- <th class="text-center" colspan="2">Actions</th> -->
                    
                  </tr>
              </thead>
              <tbody class="tablesearch1">
                <?php foreach ($route_history as $route_h) :?>
                <tr>
                    <td class="text-center"><?php echo count_id();?></td>
                    <td><?php echo remove_junk(ucfirst($route_h['date'])); ?></td>
                    <td><b><?php echo remove_junk(ucfirst($route_h['salesman'])); ?></b></td>
                    <td><?php echo remove_junk(ucfirst($route_h['coverage'])); ?></td>
                    <td><?php echo remove_junk(ucfirst($route_h['units'])); ?></td>
                    <td><?php echo remove_junk(ucfirst($route_h['loads'])); ?></td>
                    <td><?php echo remove_junk(ucfirst($route_h['returns'])); ?></td>
                    <td class="text-center">
                        <?php if($route_h['status_id'] === '1'): ?>
                          <span class="label label-success"><?php echo "On-Field"; ?></span>
                          
                        <?php elseif($route_h['status_id'] === '2'): ?>
                          <span class="label label-primary"><?php echo "Standby"; ?></span>

                        <?php elseif($route_h['status_id'] === '3'): ?>
                          <span class="label label-default"><?php echo "Cleared"; ?></span>

                        <?php endif;?>
                    </td>
                    <td class="text-center"><?php echo remove_junk(ucfirst($route_h['approver'])); ?></td>
                    <!-- <td class="text-center">
                      <div class="btn-group">
                        <a href="edit_route.php?id=<?php echo (int)$route_h['id'];?>" title="Edit" data-toggle="tooltip">
                        &nbsp;&nbsp;<i class="glyphicon glyphicon-edit"></i>&nbsp;&nbsp;
                      </a>
                      </div>
                    </td> -->
                    <!-- <td class="text-center">
                      <div class="btn-group">
                        <a href="#.php?id=<?php echo (int)$route_h['id'];?>" title="Returns" data-toggle="tooltip">
                        &nbsp;&nbsp;<i class="glyphicon glyphicon glyphicon-retweet"></i>&nbsp;&nbsp;
                      </a>
                      </div>
                    </td> -->
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
        </div>
      </div>
    </div>
   </div>
   <!-- <//?php include_once('layouts/customer_modal.php'); ?> -->
  </div>
  <?php include_once('layouts/footer.php'); ?>
  <?php include_once('includes/searchjs.php'); ?>
