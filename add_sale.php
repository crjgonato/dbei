<?php
  $page_title = 'DBEI | Add Sale';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
   page_require_level(3);
   //page_require_level(1);

   $all_customers = find_all('customers');
  
?>
<?php

  if(isset($_POST['add_sale'])){
    $req_fields = array('s_id','sales_invoice','customer','quantity','price','total', 'date' );
    validate_fields($req_fields);
        if(empty($errors)){
          $p_id      = $db->escape((int)$_POST['s_id']);
          $s_si      = $db->escape($_POST['sales_invoice']);
          $s_customer = $db->escape($_POST['customer']);
          $s_qty     = $db->escape($_POST['quantity']);
          $s_total   = $db->escape($_POST['total']);
          $s_term = $db->escape($_POST['term']);
          $date      = $db->escape($_POST['date']);
          $s_date    = make_date();

          $sql  = "INSERT INTO sales (";
          $sql .= " product_id,sales_invoice,customer,qty,price,term,date";
          $sql .= ") VALUES (";
          $sql .= "'{$p_id}','{$s_si}','{$s_customer}','{$s_qty}','{$s_total}','{$s_term}','{$s_date}'";
          $sql .= ")";

                if($db->query($sql)){
                  update_product_qty($s_qty,$p_id);
                  $session->msg('s',"Sale successfully.");
                  redirect('sales.php', false);
                } else {
                  $session->msg("d", "Sorry! failed to create a sale.");
                  redirect('add_sale.php', false);
                }
        } else {
           $session->msg("d", $errors);
           redirect('add_sale.php',false);
        }
  }

?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
<div class="row"><br>
<p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Add Sale</b></p>
  <div class="col-md-6">
    <?php echo display_msg($msg); ?>
    <form method="post" action="ajax.php" autocomplete="off" id="sug-form">
        <div class="form-group">
          <div class="input-group">
            <input type="text" id="sug_input" class="form-control input-sm initial_input" name="title" autofocus  placeholder="Please enter a product name">
<span class="input-group-btn">
              <button type="submit" class="btn btn-danger">Search</button>
            </span>
         </div>
         <div id="result" class="list-group"></div>
        </div>
    </form>
  </div>
</div>
<div class="row">

  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading clearfix">
        <strong>
          <!-- <span class="glyphicon glyphicon-th"></span> -->
          <!-- <span>Add Sale</span> -->
       </strong>
      </div>
      <div class="panel-body">
        <form method="post" action="add_sale.php">
         <table class="table table-bordered">
           <thead>
            <!-- <th> No.</th> -->
            <th> SI No.</th>
            <th> Product </th>
            <th> Salesman </th>
            <th> Customer </th>
            <th> Price </th>
            <th> Qty </th>
            <th> Total </th>
            <th> Term</th>
            <th> Date</th>
            <th> Action</th>
           </thead>
             <tbody id="product_info"></tbody>
         </table>
       </form>
      </div>
    </div>
  </div>

</div>

<?php include_once('layouts/footer.php'); ?>
