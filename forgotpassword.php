<?php
  ob_start();
  require_once('includes/load.php');
  if($session->isUserLoggedIn(true)) { redirect('home.php', false);}
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="15;url=./forgotpassword.php" />
<div class="col-md-3 pull-right noti">
    <?php echo display_msg($msg); ?>
 </div>
<div class="centered" oncontextmenu="return false;">

    <div class="text-center"><br>
    <h4>    
        <img src="dbei_logo.png" width="50" alt="DBEI Logo" draggable="false">&nbsp;
        <b><font color="#49538e">Danica</font> <font color="#cc5200">Basic Essentials Inc.</font></b>
        <!-- <h5><i><b>The Stockbook</b></i></h5> -->
    </h4>
    
       <!-- <h1>Welcome</h1> -->
       <p>Enter your email to reset password.</p>
     </div>
    
      <form method="post" action="#" class="clearfix">
        <div class="form-group">
              <label for="email" class="control-label"></label>
              <input type="email" class="form-control" name="email" placeholder="Email" autofocus>
        </div>
        
        <div class="form-group">
            <!-- <button type="submit" class="btn btn-danger  pull-left">&nbsp;&nbsp; Login &nbsp;&nbsp;</button> -->
            <p class="pull-left"><a href="index.php"> Login</a></p>
            <button type="submit" class="btn btn-danger  pull-right">&nbsp;&nbsp; Send &nbsp;&nbsp;</button>
        </div>
    </form>
    <br>
<!-- <center><p class="text-muted"><b>DBEI</b> © 2017 All rights reserved. | <a href="mailto:crjgonato@gmail.com" class="text-dark m-l-5">Feedback</a></p></center> -->
</div>
<?php include_once('layouts/footer.php'); ?>
