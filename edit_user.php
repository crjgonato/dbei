<?php
  $page_title = 'DBEI | Edit User';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
   page_require_level(1);
?>
<?php
  $e_user = find_by_id('users',(int)$_GET['id']);
  $all_employees = find_all('employees');
  $groups  = find_all('user_groups');
  if(!$e_user){
    $session->msg("d","Missing user id.");
    redirect('users.php');
  }
?>

<?php
//Update User basic info
  if(isset($_POST['update'])) {
    $req_fields = array('name','username','level');
    validate_fields($req_fields);
    if(empty($errors)){
      $id = (int)$e_user['id'];
      $name = (int)$_POST['name'];
      $username = remove_junk($db->escape($_POST['username']));
      $level = (int)$db->escape($_POST['level']);
      $status   = remove_junk($db->escape($_POST['status']));
      $date = make_date();

      $sql = "UPDATE users SET employee_id ='{$name}', username ='{$username}',user_level='{$level}',status='{$status}',date_mod='{$date}' WHERE id='{$db->escape($id)}'";
         $result = $db->query($sql);
          if($result && $db->affected_rows() === 1){
            $session->msg('s',"User account updated succesfully ");
            redirect('users.php?id='.(int)$e_user['id'], false);
          } else {
            $session->msg('d',' Sorry failed to updated!');
            redirect('edit_user.php?id='.(int)$e_user['id'], false);
          }
    } else {
      $session->msg("d", $errors);
      redirect('edit_user.php?id='.(int)$e_user['id'],false);
    }
  }
?>
<?php
// Update user password
if(isset($_POST['update-pass'])) {
  $req_fields = array('password');
  validate_fields($req_fields);
  if(empty($errors)){
     $id = (int)$e_user['id'];
     $password = remove_junk($db->escape($_POST['password']));
     $h_pass   = sha1($password);
          $sql = "UPDATE users SET password='{$h_pass}' WHERE id='{$db->escape($id)}'";
       $result = $db->query($sql);
        if($result && $db->affected_rows() === 1){
          $session->msg('s',"User password updated succesfully.");
          redirect('users.php?id='.(int)$e_user['id'], false);
        } else {
           $session->msg("d", "Sorry! failed to update.");
          redirect('edit_user.php?id='.(int)$e_user['id'], false);
        }
  } else {
    $session->msg("d", $errors);
    redirect('edit_user.php?id='.(int)$e_user['id'],false);
  }
}

?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
 <div class="row"><br>
 <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Edit User</b></p>
 <div class="col-md-3 pull-right noti">
    <?php echo display_msg($msg); ?>
 </div>
  <div class="col-md-4">
     <div class="panel panel-default">
       <div class="panel-body">
          <form method="post" action="edit_user.php?id=<?php echo (int)$e_user['id'];?>" class="clearfix">
            <div class="form-group">
                  <label for="name" class="control-label text-muted">Employee Name</label>
                  <!-- <input type="name" class="form-control input-sm name" name="name" value="<//?php echo remove_junk(ucwords($e_user['name'])); ?>"> -->
                  <select class="form-control input-sm name" name="name">
                         <?php foreach ($all_employees as $employees): ?>
                              <option value="<?php echo (int)$employees['id']; ?>" <?php if($e_user['employee_id'] === $employees['id']): echo "selected"; endif; ?> >
                              <?php echo remove_junk($employees['name']); ?></option>
                        <?php endforeach; ?>
                    </select>
            </div>
            <div class="form-group">
                  <label for="username" class="control-label text-muted">Username</label>
                  <input type="text" class="form-control input-sm name" name="username" value="<?php echo remove_junk(ucwords($e_user['username'])); ?>">
            </div>
            <div class="form-group">
              <label for="level" class="text-muted">User Role</label>
                <select class="form-control input-sm name" name="level">
                  <?php foreach ($groups as $group ):?>
                   <option <?php if($group['group_level'] === $e_user['user_level']) echo 'selected="selected"';?> value="<?php echo $group['group_level'];?>"><?php echo ucwords($group['group_name']);?></option>
                <?php endforeach;?>
                </select>
            </div>
            <div class="form-group">
              <label for="status" class="text-muted">Status</label>
                <select class="form-control input-sm name" name="status">
                  <option <?php if($e_user['status'] === '1') echo 'selected="selected"';?>value="1">Active</option>
                  <option <?php if($e_user['status'] === '0') echo 'selected="selected"';?> value="0">Deactive</option>
                </select>
            </div>
            <div class="form-group clearfix">
              <button type="button" name="cancel" class="btn btn-default btn-sm pull-left" onclick="goBack();">Cancel</button>
              <button type="submit" name="update" class="btn btn-danger btnupdate btn-sm pull-right" disabled>Apply Changes</button>      
            </div>
        </form>
       </div>
     </div>
  </div>
  <!-- Change password form -->
  <div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-body">
        <form action="edit_user.php?id=<?php echo (int)$e_user['id'];?>" method="post" class="clearfix">
          <div class="form-group">
                <label for="password" class="control-label text-muted">Password</label>
                <input type="password" class="form-control input-sm password" name="password" placeholder="Type user new password">
          </div>
          <div class="form-group clearfix">
            <button type="button" name="cancel" class="btn btn-default btn-sm pull-left" onclick="goBack();">Cancel</button>
            <button type="submit" name="update-pass" class="btn btn-danger btnupdatepassword btn-sm pull-right" disabled>Apply Changes</button> 
                  
          </div>
        </form>
      </div>
    </div>
  </div>

 </div>
<?php include_once('layouts/footer.php'); ?>
<script>
  $(document).ready(function() {
    
    //disbaled update button after any changes above inputs
    //
    $(".password").click( function(){
        $(".btnupdatepassword").prop('disabled',false);
    });
    $(".name").click( function(){
        $(".btnupdate").prop('disabled',false);
    });

  });
</script>
