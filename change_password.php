<?php
  $page_title = 'Change Password';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(3);
?>
<?php $user = current_user(); ?>
<?php
  if(isset($_POST['update'])){

    $req_fields = array('new-password','old-password','id' );
    validate_fields($req_fields);

    if(empty($errors)){

             if(sha1($_POST['old-password']) !== current_user()['password'] ){
               $session->msg('d', "Old password did not match.");
               redirect('change_password.php',false);
             }

            $id = (int)$_POST['id'];
            $new = remove_junk($db->escape(sha1($_POST['new-password'])));
            $sql = "UPDATE users SET password ='{$new}' WHERE id='{$db->escape($id)}'";
            $result = $db->query($sql);
                if($result && $db->affected_rows() === 1):
                  $session->logout();
                  $session->msg('s',"Login with your new password.");
                  redirect('index.php', false);
                else:
                  $session->msg('d',' Sorry failed to update.');
                  redirect('change_password.php', false);
                endif;
    } else {
      $session->msg("d", $errors);
      redirect('change_password.php',false);
    }
  }
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
<div class="login-page">
    <div class="text-center">
       <h3>Change your password</h3>
     </div>
     <div class="col-md-3 pull-right noti">
    <?php echo display_msg($msg); ?>
 </div>
      
</div>
<?php include_once('layouts/footer.php'); ?>
<script>
  $().ready(function() {
    
    //disbaled update button after any changes above inputs
    //
    $(".password").change( function(){
        $(".disabled").removeClass('disabled');
    });
    $(".oldpassword").change( function(){
        $(".disabled").removeClass('disabled');
    });

  });
</script>
