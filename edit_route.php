<?php
  $page_title = 'DBEI | Edit route';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
  
  //$all_customers = find_all('customers');
   $all_employees = find_all('employees');
   $all_units = find_all('unit_measures');
   $all_status = find_all('status');
?>
<?php
  //Display all routes.
  $routes = find_by_id('routes',(int)$_GET['id']);
  $all_salesman = find_all('salesman');
  
  if(!$routes){
    $session->msg("d","Missing salesman id.");
    redirect('route.php');
  }
?>
<?php
 if(isset($_POST['add_route'])){
    $req_field = array('salesman','coverage','date');
    validate_fields($req_fields);

   if(empty($errors)){
        $salesman = remove_junk($db->escape($_POST['salesman']));
        $coverage = remove_junk($db->escape($_POST['coverage']));
        $loads = remove_junk($db->escape($_POST['loads']));
        $status = remove_junk($db->escape($_POST['status']));
        ///$date = remove_junk($db->escape($_POST['date']));
        $date_mod = make_date();

      //  if (is_null($_POST['product-photo']) || $_POST['product-photo'] === "") {
      //    $media_id = '0';
      //  } else {
      //    $media_id = remove_junk($db->escape($_POST['product-photo']));
      //  }

       $query   = "UPDATE routes SET";
       $query  .=" salesman_id ='{$salesman}', coverage ='{$coverage}', loads ='{$loads}', status_id ='{$status}', date_mod ='{$date_mod}'";
       $query  .=" WHERE id ='{$routes['id']}'";
       $result = $db->query($query);
               if($result && $db->affected_rows() === 1){
                 $session->msg('s',"Route updated succesfully.");
                 redirect('route.php', false);
               } else {
                 $session->msg("d", "Sorry! failed to update.");
                 redirect('edit_route.php?id='.$routes['id'], false);
               }

   } else{
       $session->msg("d", $errors);
       redirect('edit_route.php?id='.$routes['id'], false);
   }

 }

?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
  <div class="row">
  <div class="col-md-3 pull-right noti">
  <?php echo display_msg($msg); ?>
</div>
  </div>
   <div class="row">
   <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Edit Route</b></p>
    <div class="col-md-3">
      <div class="panel panel-default">
        <div class="panel-body">
          <form method="post" action="edit_route.php?id=<?php echo (int)$routes['id'];?>">  
           <div class="form-group ">
                 <p>Salesman</p>
                  <select class="form-control input-sm route_salemsan" name="salesman">
                     <?php  foreach ($all_employees as $employees): ?>
                        <option value="<?php echo (int)$employees['id']; ?>" <?php if($routes['salesman_id'] === $employees['id']): echo "selected"; endif; ?> >
                        <?php echo remove_junk($employees['name']); ?></option>
                     <?php endforeach; ?>
                  </select> 
                
           </div>
            <div class="form-group">
              <p>Quantity</p>
              <input type="number" name="loads" class="form-control input-sm route_loads" value="<?php echo remove_junk(ucfirst($routes['loads']));?>">
            </div>
            <div class="form-group">
               <p>Coverage</p>
               
                   <textarea class="form-control input-sm route_coverage" name="coverage" placeholder="Coverage" require="required"><?php echo remove_junk(ucfirst($routes['coverage']));?></textarea>
            </div>
           <div class="form-group ">
                 <p>Unit</p>
                  <select class="form-control input-sm route_salemsan" name="units">
                     <?php  foreach ($all_units as $units): ?>
                        <option value="<?php echo (int)$units['id']; ?>" <?php if($routes['unit_id'] === $units['id']): echo "selected"; endif; ?> >
                        <?php echo remove_junk($units['name']); ?></option>
                     <?php endforeach; ?>
                  </select> 
                
           </div>
            <div class="form-group">
               <p>Date</p>
                <input type="text" class="form-control input-sm route_date datepicker" name="date" placeholder="Date" value="<?php echo remove_junk(ucfirst($routes['date']));?>" require="required"> 
            </div>
            <div class="form-group ">
                 <p>Status</p>
                  <!-- <select class="form-control input-sm route_salemsan" name="salesman">
                     <//?php  foreach ($all_units as $units): ?>
                        <option value="<//?php echo (int)$units['id']; ?>" <//?php if($routes['unit_id'] === $units['id']): echo "selected"; endif; ?> >
                        <//?php echo remove_junk($units['name']); ?></option>
                     <//?php endforeach; ?>
                  </select>  -->
                  <select class="form-control input-sm route_salemsan" name="status">
                    <!-- <option>Select</option> -->
                    <?php  foreach ($all_status as $status): ?>
                        <option value="<?php echo (int)$status['id']; ?>" <?php if($routes['status_id'] === $status['id']): echo "selected"; endif; ?> >
                        <?php echo remove_junk($status['name']); ?></option>
                     <?php endforeach; ?>
                  </select>
           </div>
           <div class="form-group ">
                 <p>Approver</p>
                  <select class="form-control input-sm route_salemsan" name="approver">
                    <option><?php echo remove_junk($routes['approver']); ?></option>
                    <?php  foreach ($all_employees as $employees): ?>
                        <option value="<?php echo (int)$employees['name']; ?>" <?php if($routes['salesman_id'] === $employees['id']):  endif; ?> >
                        <?php echo remove_junk($employees['name']); ?></option>
                     <?php endforeach; ?>
                     
                  </select> 
           </div>
            <button type="button" name="cancel" class="btn btn-default btn-sm pull-left" onclick="goBack();">Cancel</button>
            <button type="submit" name="add_route" class="btn btn-danger btn-sm btnupdate pull-right" disabled>Apply Changes</button>
            
        </form>
        </div>
      </div>
    </div>
    
   </div>
   <!-- <//?php include_once('layouts/customer_modal.php'); ?> -->
  </div>
  <?php include_once('layouts/footer.php'); ?>
<script>
  $(document).ready(function() {
    //disbaled update button after any changes above inputs
    //
    $(".route_salemsan").change( function(){
        $(".btnupdate").prop('disabled',false);
    });
    $(".route_coverage").focus( function(){
        $(".btnupdate").prop('disabled',false);
    });
    $(".route_loads").focus( function(){
        $(".btnupdate").prop('disabled',false);
    });
    $(".route_date").change( function(){
        $(".btnupdate").prop('disabled',false);
    });
    
  });
</script>
