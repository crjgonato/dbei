<?php
  $page_title = 'DBEI | Edit sale';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
   page_require_level(3);
?>
<?php
$sale = find_by_id('sales',(int)$_GET['id']);
if(!$sale){
  $session->msg("d","Missing product id.");
  redirect('sales.php');
}
?>
<?php $product = find_by_id('products',$sale['product_id']); ?>
<?php

  if(isset($_POST['update_sale'])){
    $req_fields = array('title','quantity','price','total', 'date' );
    validate_fields($req_fields);
        if(empty($errors)){
          $p_id      = $db->escape((int)$product['id']);
          $s_qty     = $db->escape($_POST['quantity']);
          $s_total   = $db->escape($_POST['total']);
          $date      = $db->escape($_POST['date']);
          $s_date    = date("Y-m-d", strtotime($date));

          $sql  = "UPDATE sales SET";
          $sql .= " product_id= '{$p_id}',qty={$s_qty},price='{$s_total}',date='{$s_date}'";
          $sql .= " WHERE id ='{$sale['id']}'";
          $result = $db->query($sql);
          if( $result && $db->affected_rows() === 1){
                    update_product_qty($s_qty,$p_id);
                    $session->msg('s',"Sale updated succesfully..");
                    redirect('edit_sale.php?id='.$sale['id'], false);
                  } else {
                    $session->msg("d", "Sorry! failed to update.");
                    redirect('sales.php', false);
                  }
        } else {
           $session->msg("d", $errors);
           redirect('edit_sale.php?id='.(int)$sale['id'],false);
        }
  }

?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
<div class="row">
<div class="col-md-3 pull-right noti">
<?php echo display_msg($msg); ?>
</div>
</div>
<div class="row">

  <div class="col-md-12">
  <div class="panel">
    <div class="panel-heading clearfix">
      <strong>
        <!-- <span class="glyphicon glyphicon-th"></span> -->
        <span>Edit Sale</span>
     </strong>
     <div class="pull-right">
       <a href="sales.php" class="btn btn-danger">All Sales</a>
     </div>
    </div>
    <div class="panel-body">
       <table class="table table-bordered">
         <thead>
          <th> Product Name </th>
          <th> Quantity </th>
          <th> Price </th>
          <th> Total </th>
          <th> Date</th>
          <th> Action</th>
         </thead>
           <tbody  id="product_info">
              <tr>
              <form method="post" action="edit_sale.php?id=<?php echo (int)$sale['id']; ?>">
                <td id="s_name" style="width: 35%;">
                  <input type="text" class="form-control initial_input prdct_name" id="sug_input" name="title" value="<?php echo remove_junk($product['name']); ?>">
                  <div id="result" class="list-group"></div>
                </td>
                <td id="s_qty" style="width: 12%;">
                  <input type="text" class="form-control" name="quantity" value="<?php echo remove_junk($sale['qty']); ?>" readonly style="cursor:not-allowed;">
                </td>
                <td id="s_price" style="width: 12%;">
                  <input type="text" class="form-control" name="price" value="<?php echo remove_junk($product['sale_price']); ?>" readonly style="cursor:not-allowed;">
                </td>
                <td style="width: 12%;">
                  <input type="text" class="form-control" name="total" value="<?php echo remove_junk($sale['price']); ?>" readonly style="cursor:not-allowed;">
                </td>
                <td id="s_date" style="width: 12%;">
                  <input type="date" class="form-control" name="date" data-date-format="" value="<?php echo remove_junk($sale['date']); ?>" readonly style="cursor:not-allowed;">
                </td>
                <td style="width: 14%;">
                  <button type="submit" name="update_sale" class="btn btn-danger btnupdate" disabled>Save</button>
                  <button type="button" name="cancel" class="btn btn-default pull-right" onclick="goBack();">Cancel</button>
                </td>
              </form>
              </tr>
           </tbody>
       </table>

    </div>
  </div>
  </div>

</div>

<?php include_once('layouts/footer.php'); ?>
<script>
  $(document).ready(function() {
    //disbaled update button after any changes above inputs
    //
    $(".prdct_name").focus( function(){
        $(".btnupdate").prop('disabled',false);
    });
  });
</script>
