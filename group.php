<?php
  $page_title = 'DBEI | Groups';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
   page_require_level(1);
  $all_groups = find_all('user_groups');
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
<div class="row">
<div class="col-md-3 pull-right noti">
<?php echo display_msg($msg); ?>
</div>
</div>
<div class="row">
  <ol class="breadcrumb pull-right">
        <li><a href="admin.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Groups</li>
      </ol>
<p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Groups (User Level)</b></p>
  <div class="col-md-12">
    <div class="panel panel-default">
    <div class="panel-heading clearfix">
    <div class="col-sm-4 pull-left input-group">
            <input type="text" class="form-control input-sm" id="search" placeholder="Search group here..">
              <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
          </div>
      <strong>
        <!-- <span class="glyphicon glyphicon-th"></span> -->
        <!-- <span>Groups</span> -->
     </strong>
       <a href="add_group.php" class="btn btn-danger input-xs pull-right btn-sm"> Add New Group</a>
    </div>
     <div class="panel-body">
      <table class="table table-bordered table-condensed">
        <thead>
          <tr>
            <th class="text-center" style="width: 50px;">#</th>
            <th>Group Names</th>
            <th class="text-center" style="width: 20%;">Group Level</th>
            <th class="text-center" style="width: 15%;">Status</th>
            <th class="text-center" style="width: 100px;">Actions</th>
          </tr>
        </thead>
        <tbody class="tablesearch">
        <?php foreach($all_groups as $a_group): ?>
          <tr>
           <td class="text-center"><?php echo count_id();?></td>
           <td><?php echo remove_junk(ucwords($a_group['group_name']))?></td>
           <td class="text-center">
             <?php echo remove_junk(ucwords($a_group['group_level']))?>
           </td>
           <td class="text-center">
           <?php if($a_group['group_status'] === '1'): ?>
            <span class="label label-success"><?php echo "Active"; ?></span>
          <?php else: ?>
            <span class="label label-primary"><?php echo "Inactive"; ?></span>
          <?php endif;?>
           </td>
           <td class="text-center">
             <div class="btn-group">
                <a href="edit_group.php?id=<?php echo (int)$a_group['id'];?>" data-toggle="tooltip" title="Edit">
                &nbsp;&nbsp;<i class="glyphicon glyphicon-edit"></i>&nbsp;&nbsp;
               </a>
                <!-- <a href="delete_group.php?id=<?php echo (int)$a_group['id'];?>" class="btn btn-xs btn-danger" title="Remove" data-toggle="tooltip">
                &nbsp;&nbsp;<i class="glyphicon glyphicon-trash"></i>&nbsp;&nbsp;
                </a> -->
                </div>
           </td>
          </tr>
        <?php endforeach;?>
       </tbody>
     </table>
     </div>
    </div>
  </div>
  <?php include_once('layouts/group_modal.php'); ?>
</div>
  <?php include_once('layouts/footer.php'); ?>
  <?php include_once('includes/searchjs.php'); ?>
