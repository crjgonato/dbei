<?php
  $page_title = 'DBEI | Edit Employee';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
?>
<?php
  //Display all catgories.
  $employees = find_by_id('employees',(int)$_GET['id']);
  if(!$employees){
    $session->msg("d","Missing code id.");
    redirect('employees.php');
  }
?>

<?php
if(isset($_POST['edit_employees'])){
  $req_field = array('edit_employee');
  validate_fields($req_field);
  $employee = remove_junk($db->escape($_POST['edit_employee']));
  $age = remove_junk($db->escape($_POST['edit_age']));
  $gender = remove_junk($db->escape($_POST['edit_gender']));
  $date = make_date();

  if(empty($errors)){
      $sql = "UPDATE employees SET name='{$employee}',age='{$age}',gender='{$gender}',date_mod='{$date}'";
      $sql .= " WHERE id='{$employees['id']}'";
     
      $result = $db->query($sql);
     if($result && $db->affected_rows() === 1) {
       $session->msg("s", "Employee updated succesfully.");
       redirect('employees.php',false);
     } else {
       $session->msg("d", "Sorry! failed to update.");
       redirect('employees.php',false);
     }
  } else {
    $session->msg("d", $errors);
    redirect('employees.php',false);
  }
}
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
<div class="row">
<div class="col-md-3 pull-right noti">
<?php echo display_msg($msg); ?>
</div><br>
<p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Edit Employee</b></p>
   <div class="col-md-3">
     <div class="panel panel-default">
       <div class="panel-body">
         <form method="post" action="edit_employees.php?id=<?php echo (int)$employees['id'];?>">
           <div class="form-group">
            <p>Employee name</p>
               <input type="text" class="form-control input-sm employee code" name="edit_employee" value="<?php echo remove_junk(ucfirst($employees['name']));?>">
           </div>
           <div class="form-group col-sm-6">
           <p>Age</p>
                <input type="number" class="form-control input-sm employee" name="edit_age" placeholder="Age" value="<?php echo remove_junk(ucfirst($employees['age']));?>"> 
            </div>
            <div class="form-group col-sm-6">
             <p>Gender</p>
              <select class="form-control input-sm employee" name="edit_gender">
                  <option value="<?php echo remove_junk(ucfirst($employees['gender']));?>" selected><?php echo remove_junk(ucfirst($employees['gender']));?></option>
                  <option value="Male">Male</option>
                  <option value="Female">Female</option>
               </select>
            </div>
          <button type="button" name="cancel" class="btn btn-default btn-sm pull-left" onclick="goBack();">Cancel</button>
           <button type="submit" name="edit_employees" class="btn btn-danger btn-sm btnupdate pull-right" disabled>Apply Changes</button>
       </form>
       </div>
     </div>
   </div>
</div>



<?php include_once('layouts/footer.php'); ?>
<script>
  $(document).ready(function() {
    //disbaled update button after any changes above inputs
    //
    $(".employee").focus( function(){
        $(".btnupdate").prop('disabled',false);
    });
  });
</script>
