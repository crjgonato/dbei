<?php
  $page_title = 'DBEI | Edit code';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
?>
<?php
  //Display all catgories.
  $units = find_by_id('unit_measures',(int)$_GET['id']);
  if(!$units){
    $session->msg("d","Missing unit id.");
    redirect('measurements.php');
  }
?>

<?php
if(isset($_POST['edit_units'])){
  $req_field = array('product-units');
  validate_fields($req_field);
  $units_name = remove_junk($db->escape($_POST['product-units']));
  $date = make_date();
  if(empty($errors)){
        $sql = "UPDATE unit_measures SET name='{$units_name}', date_mod='{$date}'";
       $sql .= " WHERE id='{$units['id']}'";
     $result = $db->query($sql);
     if($result && $db->affected_rows() === 1) {
       $session->msg("s", "Units updated succesfully.");
       redirect('measurements.php',false);
     } else {
       $session->msg("d", "Sorry! failed to update.");
       redirect('measurements.php',false);
     }
  } else {
    $session->msg("d", $errors);
    redirect('measurements.php',false);
  }
}
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
<div class="row">
<div class="col-md-3 pull-right noti">
<?php echo display_msg($msg); ?>
</div>
<br>
<p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Edit Unit</b></p>
   <div class="col-md-3">
     <div class="panel panel-default">
       <div class="panel-body">
         <form method="post" action="edit_measurements.php?id=<?php echo (int)$units['id'];?>">
           <div class="form-group">
           <p>Unit name</p>
               <input type="text" class="form-control input-sm unit" name="product-units" value="<?php echo remove_junk(ucfirst($units['name']));?>">
           </div>
           <button type="button" name="cancel" class="btn btn-default btn-sm pull-left" onclick="goBack();">Cancel</button>
           <button type="submit" name="edit_units" class="btn btn-danger btn-sm btnupdate pull-right" disabled>Apply Changes</button>
            
       </form>
       </div>
     </div>
   </div>
</div>



<?php include_once('layouts/footer.php'); ?>
<script>
  $().ready(function() {
    //disbaled update button after any changes above inputs
    //
    $(".unit").focus( function(){
        $(".btnupdate").prop('disabled',false);
    });
  });
</script>

