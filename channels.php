<?php
  $page_title = 'DBEI | Channels';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
  
  $all_channels = find_channels();
?>
<?php
 if(isset($_POST['add_channels'])){
   $req_field = array('channels-name');
   validate_fields($req_field);
      $channels_name = remove_junk($db->escape($_POST['channels-name']));
      $date = make_date();
   if(empty($errors)){
      $sql  = "INSERT INTO channels (name,date)";
      $sql .= " VALUES ('{$channels_name}','{$date}')";

      if($db->query($sql)){
        $session->msg("s", "Channel created successfully.");
        redirect('channels.php',false);
      } else {
        $session->msg("d", "Sorry! failed to create.");
        redirect('channels.php',false);
      }
   } else {
     $session->msg("d", $errors);
     redirect('channels.php',false);
   }
 }
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
  <div class="row">
  <div class="col-md-3 pull-right noti">
  <?php echo display_msg($msg); ?>
</div>
  </div>
   <div class="row">
     <ol class="breadcrumb pull-right">
        <li><a href="admin.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Channels</li>
      </ol>
   <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Channels</b></p>
    <div class="col-md-3">
      <div class="panel panel-default">
        <div class="panel-body">
          <form method="post" action="channels.php">
          <div class="form-group">
                <input type="text" class="form-control input-sm" name="channels-name" placeholder="New Channel" autofocus require="required"> 
            </div>

            <button type="submit" name="add_channels" class="btn btn-danger btn-sm pull-right">Add Channel</button>
        </form>
        </div>
      </div>
    </div>
    <div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-body">
        <div class="col-sm-4 pull-right input-group">
            <input type="text" class="form-control input-sm" id="search" placeholder="Search channels here..">
              <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
          </div>
          <table class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th class="text-center" style="width: 50px;">#</th>
                    <th>Channels</th>
                    <th>Date Added</th>
                    <th class="text-center" style="width: 100px;">Actions</th>
                </tr>
            </thead>
            <tbody class="tablesearch">
              <?php foreach ($all_channels as $channels):?>
              <tr>
                  <td class="text-center"><?php echo count_id();?></td>
                  
                  <td><?php echo remove_junk(ucfirst($channels['name'])); ?></td>
                  <td><?php echo remove_junk(ucfirst($channels['date'])); ?></td>
                  <td class="text-center">
                    <div class="btn-group">
                      <!-- <a href="edit_customer.php?id=<//?php echo (int)$customers['id'];?>"  class="btn btn-xs btn-warning" data-toggle="tooltip" title="Edit"> -->
                      <!-- <a href="#"  class="btn btn-xs btn-warning" data-toggle="tooltip" title="Edit">
                      &nbsp;&nbsp;<i class="glyphicon glyphicon-edit"></i>&nbsp;&nbsp;
                      </a> -->
                      <a href="edit_channels.php?id=<?php echo (int)$channels['id'];?>" title="Edit" data-toggle="tooltip">
                      &nbsp;&nbsp;<i class="glyphicon glyphicon-edit"></i>&nbsp;&nbsp;
                    </a>
                      <!-- <a href="#?id=<//?php echo (int)$customers['id'];?>"  class="btn btn-xs btn-danger" title="Remove" data-toggle="tooltip">
                      &nbsp;&nbsp;<i class="glyphicon glyphicon-trash"></i>&nbsp;&nbsp;
                      </a> -->
                    </div>
                  </td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
       </div>
    </div>
    </div>
   </div>
   <!-- <//?php include_once('layouts/customer_modal.php'); ?> -->
  </div>
  <?php include_once('layouts/footer.php'); ?>
  <?php include_once('includes/searchjs.php'); ?>
