<?php
  require_once('includes/load.php');

/*--------------------------------------------------------------*/
/* Function for find all database table rows by table name
/*--------------------------------------------------------------*/
  function find_all($table) {
   global $db;
   if(tableExists($table))
   {
     return find_by_sql("SELECT * FROM ".$db->escape($table));
   }
}
/*--------------------------------------------------------------*/
/* Function for Perform queries
/*--------------------------------------------------------------*/
function find_by_sql($sql)
{
  global $db;
  $result = $db->query($sql);
  $result_set = $db->while_loop($result);
 return $result_set;
}
/*--------------------------------------------------------------*/
/*  Function for Find data from table by id
/*--------------------------------------------------------------*/
function find_by_id($table,$id)
{
  global $db;
  $id = (int)$id;
    if(tableExists($table)){
          $sql = $db->query("SELECT * FROM {$db->escape($table)} WHERE id='{$db->escape($id)}' LIMIT 1");
          if($result = $db->fetch_assoc($sql))
            return $result;
          else
            return null;
     }
}
/*--------------------------------------------------------------*/
/* Function for Delete data from table by id
/*--------------------------------------------------------------*/
function delete_by_id($table,$id)
{
  global $db;
  if(tableExists($table))
   {
    $sql = "DELETE FROM ".$db->escape($table);
    $sql .= " WHERE id=". $db->escape($id);
    $sql .= " LIMIT 1";
    $db->query($sql);
    return ($db->affected_rows() === 1) ? true : false;
   }
}
/*--------------------------------------------------------------*/
/* Function for Count id  By table name
/*--------------------------------------------------------------*/

function count_by_id($table){
  global $db;
  if(tableExists($table))
  {
    $sql    = "SELECT COUNT(id) AS total FROM ".$db->escape($table);
    $result = $db->query($sql);
     return($db->fetch_assoc($result));
  }
}
/*--------------------------------------------------------------*/
/* Determine if database table exists
/*--------------------------------------------------------------*/
function tableExists($table){
  global $db;
  $table_exit = $db->query('SHOW TABLES FROM '.DB_NAME.' LIKE "'.$db->escape($table).'"');
      if($table_exit) {
        if($db->num_rows($table_exit) > 0)
              return true;
         else
              return false;
      }
  }
 /*--------------------------------------------------------------*/
 /* Login with the data provided in $_POST,
 /* coming from the login form.
/*--------------------------------------------------------------*/
  function authenticate($username='', $password='') {
    global $db;
    $username = $db->escape($username);
    $password = $db->escape($password);
    $sql  = sprintf("SELECT id,username,password,user_level FROM users WHERE username ='%s' LIMIT 1", $username);
    $result = $db->query($sql);
    if($db->num_rows($result)){
      $user = $db->fetch_assoc($result);
      $password_request = sha1($password);
      if($password_request === $user['password'] ){
        return $user['id'];
      }
    }
   return false;
  }
  /*--------------------------------------------------------------*/
  /* Login with the data provided in $_POST,
  /* coming from the login_v2.php form.
  /* If you used this method then remove authenticate function.
 /*--------------------------------------------------------------*/
   function authenticate_v2($username='', $password='') {
     global $db;
     $username = $db->escape($username);
     $password = $db->escape($password);
     $sql  = sprintf("SELECT id,username,password,user_level FROM users WHERE username ='%s' LIMIT 1", $username);
     $result = $db->query($sql);
     if($db->num_rows($result)){
       $user = $db->fetch_assoc($result);
       $password_request = sha1($password);
       if($password_request === $user['password'] ){
         return $user;
       }
     }
    return false;
   }


  /*--------------------------------------------------------------*/
  /* Find current log in user by session id
  /*--------------------------------------------------------------*/
  function current_user(){
      static $current_user;
      global $db;
      if(!$current_user){
         if(isset($_SESSION['user_id'])):
             $user_id = intval($_SESSION['user_id']);
             $current_user = find_by_id('users',$user_id);
        endif;
      }
    return $current_user;
  }
  /*--------------------------------------------------------------*/
  /* Find all user by
  /* Joining users table and user gropus table
  /*--------------------------------------------------------------*/
  function find_all_user(){
      global $db;
      $results = array();
      $sql = "SELECT u.id,u.username,u.user_level,u.status,u.last_login,";
      $sql .="g.group_name,e.name AS name ";
      $sql .="FROM users u ";
      $sql .="LEFT JOIN user_groups g ON g.group_level=u.user_level ";
      $sql .="LEFT JOIN employees e ON e.id = u.employee_id ";
      $sql .="ORDER BY u.employee_id DESC";
      $result = find_by_sql($sql);
      return $result;
  }
  /*--------------------------------------------------------------*/
  /* Function to update the last log in of a user
  /*--------------------------------------------------------------*/

 function updateLastLogIn($user_id)
	{
		global $db;
    $date = make_date();
    $sql = "UPDATE users SET last_login='{$date}' WHERE id ='{$user_id}' LIMIT 1";
    $result = $db->query($sql);
    return ($result && $db->affected_rows() === 1 ? true : false);
  }

// update Log
// function insertLogsLogIn($user_id)
// 	{
// 		global $db;
//     $date = make_date();
//     $sql = "UPDATE logs SET logs_date'{$date}' WHERE id ='{$user_id}' LIMIT 1";
//     $result = $db->query($sql);
//     return ($result && $db->affected_rows() === 1 ? true : false);
// 	}

  /*--------------------------------------------------------------*/
  /* Find all Group name
  /*--------------------------------------------------------------*/
  function find_by_groupName($val)
  {
    global $db;
    $sql = "SELECT group_name FROM user_groups WHERE group_name = '{$db->escape($val)}' LIMIT 1 ";
    $result = $db->query($sql);
    return($db->num_rows($result) === 0 ? true : false);
  }
  /*--------------------------------------------------------------*/
  /* Find group level
  /*--------------------------------------------------------------*/
  function find_by_groupLevel($level)
  {
    global $db;
    $sql = "SELECT group_level FROM user_groups WHERE group_level = '{$db->escape($level)}' LIMIT 1 ";
    $result = $db->query($sql);
    return($db->num_rows($result) === 0 ? true : false);
  }
  /*--------------------------------------------------------------*/
  /* Function for cheaking which user level has access to page
  /*--------------------------------------------------------------*/
   function page_require_level($require_level){
     global $session;
     $current_user = current_user();
     $login_level = find_by_groupLevel($current_user['user_level']);
     //if user not login
     if (!$session->isUserLoggedIn(true)):
            $session->msg('d','Please login your account.');
            redirect('index.php', false);

      //if Group status Inactive
     elseif($login_level['group_status'] === '0'):
           $session->msg('d','This level user has been band!');
           redirect('index.php',false);
           
      //cheackin log in User level and Require level is Less than or equal to
     elseif($current_user['user_level'] <= (int)$require_level):
              return true;
      else:
            $session->msg("d", "Sorry! you dont have permission to view the page.");
            redirect('index.php', false);
        endif;

     }
   /*--------------------------------------------------------------*/
   /* Function for Finding all product name
   /* JOIN with categorie, product code and media database table
   /*--------------------------------------------------------------*/
  function join_product_table(){
     global $db;
    //  $sql  =" SELECT p.id,p.code,p.name,p.quantity,p.uom,p.buy_price,p.sale_price,p.media_id,p.date,c.name";
    $sql  =" SELECT p.id,p.name,p.quantity,p.buy_price,p.sale_price,p.media_id,p.date,c.name";
    $sql  .=" AS categorie,m.file_name AS image,x.name AS code,u.name AS unit";
    $sql  .=" FROM products p";
    $sql  .=" LEFT JOIN categories c ON c.id = p.categorie_id";
    $sql  .=" LEFT JOIN codes x ON x.id = p.code_id";
    $sql  .=" LEFT JOIN unit_measures u ON u.id = p.unit_id";
    $sql  .=" LEFT JOIN media m ON m.id = p.media_id";
    $sql  .=" ORDER BY p.id DESC";
    return find_by_sql($sql);

   }
   /*--------------------------------------------------------------*/
   /* Function for Finding all product name
   /* JOIN with categorie, product code and media database table
   /*--------------------------------------------------------------*/
  // function join_product_name_table(){
  //    global $db;
  //   //  $sql  =" SELECT p.id,p.code,p.name,p.quantity,p.uom,p.buy_price,p.sale_price,p.media_id,p.date,c.name";
  //   $sql  =" SELECT t.id,t.name,t.quantity,t.buy_price,t.sale_price,t.media_id,t.date,t.product_id";
  //   $sql  .=" AS tproduct,m.file_name AS image,x.name AS code,u.name AS unit";
  //   $sql  .=" FROM product_cart t";
  //   $sql  .=" LEFT JOIN categories c ON c.id = t.categorie_id";
  //   $sql  .=" LEFT JOIN products p ON p.id = t.product_id";
  //   $sql  .=" LEFT JOIN codes x ON x.id = t.code_id";
  //   $sql  .=" LEFT JOIN unit_measures u ON u.id = t.unit_id";
  //   $sql  .=" LEFT JOIN media m ON m.id = t.media_id";
  //   $sql  .=" ORDER BY t.id DESC";
  //   return find_by_sql($sql);
  //  }
  function join_product_name_table(){
     global $db;
    $sql  =" SELECT t.id,t.name,t.sale_price,t.product_id";
    $sql  .=" AS tproduct";
    $sql  .=" FROM product_cart t";
    $sql  .=" LEFT JOIN products p ON p.id = t.product_id";
    $sql  .=" ORDER BY t.id DESC";
    return find_by_sql($sql);

   }
   /*--------------------------------------------------------------*/
   /* Function for Finding all customer name
   /* JOIN with channels database tables
   /*--------------------------------------------------------------*/
  function join_customers_table(){
     global $db;
    $sql  =" SELECT c.id,c.codename,c.businessname,c.name,c.gender,c.balance,c.contact,c.zipcode,c.street,c.brgy,c.city,c.date,x.name";
    $sql  .=" AS channels";
    $sql  .=" FROM customers c";
    $sql  .=" LEFT JOIN channels x ON x.id = c.channels_id";
    $sql  .=" ORDER BY c.id DESC";
    return find_by_sql($sql);
   }

   /*--------------------------------------------------------------*/
   /* Function for Finding all product code
   /*--------------------------------------------------------------*/
  function find_prcode(){
     global $db;
    $sql  =" SELECT c.id,c.name,c.date";
    $sql  .=" FROM codes c";
    $sql  .=" ORDER BY c.id DESC";
    return find_by_sql($sql);
   }

  /*--------------------------------------------------------------*/
  /* Function for Finding all employee
  /*--------------------------------------------------------------*/
  function find_employee(){
     global $db;
    $sql  =" SELECT e.id,e.name,e.age,e.gender";
    $sql  .=" FROM employees e";
    $sql  .=" ORDER BY e.id DESC";
    return find_by_sql($sql);
   }

   /*--------------------------------------------------------------*/
   /* Function for Finding all Expenses Desc
   /*--------------------------------------------------------------*/
  function find_exp_desc(){
     global $db;
    $sql  =" SELECT e.id,e.name,e.date";
    $sql  .=" FROM expenses_desc e";
    $sql  .=" ORDER BY e.id DESC";
    return find_by_sql($sql);
   }

   /*--------------------------------------------------------------*/
   /* Function for Finding all Expenses Desc
   /*--------------------------------------------------------------*/
  function find_exp_account(){
     global $db;
    $sql  =" SELECT a.id,a.name,a.date";
    $sql  .=" FROM expenses_account a";
    $sql  .=" ORDER BY a.id DESC";
    return find_by_sql($sql);
   }

   /*--------------------------------------------------------------*/
   /* Function for Finding all unit
   /*--------------------------------------------------------------*/
  function find_uom(){
     global $db;
    $sql  =" SELECT u.id,u.name,u.date";
    $sql  .=" FROM unit_measures u";
    $sql  .=" ORDER BY u.id DESC";
    return find_by_sql($sql);
   }

   /*--------------------------------------------------------------*/
   /* Function for Finding all salesman
   /*--------------------------------------------------------------*/
  function find_salesman(){
     global $db;
    $sql  =" SELECT s.id,s.platenumber,e.name ";
    $sql .= "AS name ";
    $sql .=" FROM salesman s ";
    $sql .="LEFT JOIN employees e ON e.id = s.employee_id ";
    $sql .=" ORDER BY s.id DESC";
    return find_by_sql($sql);
   }

  //   /*--------------------------------------------------------------*/
  //  /* Function for Finding all routes
  //  /*--------------------------------------------------------------*/
  // function find_routes(){
  //    global $db;
  //   $sql  =" SELECT r.id,r.coverage,r.date,e.name";
  //   $sql .= "AS name ";
  //   $sql .=" FROM routes r ";
  //   $sql .="LEFT JOIN employees e ON e.id = s.employee_id ";
  //   $sql .=" ORDER BY s.id DESC";
  //   return find_by_sql($sql);
  //  }
   
   /*--------------------------------------------------------------*/
   /* Function for Finding all routes
   /*--------------------------------------------------------------*/
  function find_supplier(){
     global $db;
    $sql  =" SELECT l.id,l.name,l.address,l.contacts,l.vatregtine";
    $sql  .=" FROM supplier l";
    $sql  .=" ORDER BY l.id DESC";
    return find_by_sql($sql);
   }
   /*--------------------------------------------------------------*/
   /* Function for Finding all channels
   /*--------------------------------------------------------------*/
  function find_channels(){
     global $db;
    $sql  =" SELECT c.id,c.name,c.date";
    $sql  .=" FROM channels c";
    $sql  .=" ORDER BY c.id DESC";
    return find_by_sql($sql);
   }

   /*--------------------------------------------------------------*/
   /* Function for Finding all logs
   /*--------------------------------------------------------------*/
  function find_logs(){
     global $db;
    $sql  =" SELECT l.id,l.action,l.name,l.logs_date";
    $sql  .=" FROM logs l";
    $sql  .=" ORDER BY l.id DESC";
    return find_by_sql($sql);

   }
/*--------------------------------------------------------------*/
   /* Function for Finding all logs
   /*--------------------------------------------------------------*/
  function find_updates(){
     global $db;
    $sql  =" SELECT u.id,u.name,u.date";
    $sql  .=" FROM updates u";
    $sql  .=" ORDER BY u.id DESC";
    return find_by_sql($sql);

   }
   /*--------------------------------------------------------------*/
   /* Function for Finding all customer name
   /* JOIN with channels database table
   /*--------------------------------------------------------------*/
  function join_route_table(){
     global $db;
    $sql  =" SELECT r.id,r.coverage,r.loads,r.status_id,r.returns,r.approver,r.date,x.name";
    $sql  .=" AS salesman,u.name AS units";
    $sql  .=" FROM routes r";
    $sql  .=" LEFT JOIN employees x ON x.id = r.salesman_id";
    $sql  .=" LEFT JOIN unit_measures u ON u.id = r.unit_id";
    // $sql  .=" LEFT JOIN status s ON s.id = r.status_id";
    $sql  .=" ORDER BY r.date DESC";
    return find_by_sql($sql);

   }
   /*--------------------------------------------------------------*/
   /* Function for Finding all customer name
   /* JOIN with channels database table
   /*--------------------------------------------------------------*/
  function join_route_history_table(){
     global $db;
    $sql  =" SELECT r.id,r.coverage,r.loads,r.status_id,r.returns,r.approver,r.date,x.name";
    $sql  .=" AS salesman,u.name AS units";
    $sql  .=" FROM route_history r";
    $sql  .=" LEFT JOIN employees x ON x.id = r.salesman_id";
    $sql  .=" LEFT JOIN unit_measures u ON u.id = r.unit_id";
    // $sql  .=" LEFT JOIN status s ON s.id = r.status_id";
    $sql  .=" ORDER BY r.date DESC";
    return find_by_sql($sql);

   }
  /*--------------------------------------------------------------*/
  /* Function for Finding all product name
  /* Request coming from ajax.php for auto suggest
  /*--------------------------------------------------------------*/
   function find_product_by_title($product_name){
     global $db;
     $p_name = remove_junk($db->escape($product_name));
    //  $sql = "SELECT name FROM products WHERE name like '%$p_name%' LIMIT 5";
     $sql = "SELECT name FROM products WHERE name like '%$p_name%'";
     $result = find_by_sql($sql);
     return $result;
   }
/*--------------------------------------------------------------*/
  /* Function for Finding all Saleman name
  /* Request coming from ajax.php
  /*--------------------------------------------------------------*/

  function find_salesman_by_name($sales_name){
    global $db;
    $l_name = remove_junk($db->escape($sales_man));
   //  $sql = "SELECT name FROM products WHERE name like '%$p_name%' LIMIT 5";
    $sql = "SELECT name FROM salesman WHERE name like '%$l_name%'";
    $result = find_by_sql($sql);
    return $result;
  }
  /*--------------------------------------------------------------*/
  /* Function for Finding all product info by product title
  /* Request coming from ajax.php
  /*--------------------------------------------------------------*/
  function find_all_product_info_by_title($title){
    global $db;
    $sql  = "SELECT * FROM products ";
    $sql .= " WHERE name ='{$title}'";
    $sql .=" LIMIT 1";
    return find_by_sql($sql);
  }

  /*--------------------------------------------------------------*/
  /* Function for Update product quantity
  /*--------------------------------------------------------------*/
  function update_product_qty($qty,$p_id){
    global $db;
    $qty = (int) $qty;
    $id  = (int)$p_id;
    $sql = "UPDATE products SET quantity=quantity -'{$qty}' WHERE id = '{$id}'";
    $result = $db->query($sql);
    return($db->affected_rows() === 1 ? true : false);

  }
  /*--------------------------------------------------------------*/
  /* Function for Display Recent product Added
  /*--------------------------------------------------------------*/
 function find_recent_product_added($limit){
   global $db;
   $sql   = " SELECT p.id,p.name,p.sale_price,p.media_id,c.name AS categorie,";
   $sql  .= "m.file_name AS image FROM products p";
   $sql  .= " LEFT JOIN categories c ON c.id = p.categorie_id";
   $sql  .= " LEFT JOIN media m ON m.id = p.media_id";
   $sql  .= " ORDER BY p.id DESC LIMIT ".$db->escape((int)$limit);
   return find_by_sql($sql);
 }
 /*--------------------------------------------------------------*/
 /* Function for Find Highest saleing Product
 /*--------------------------------------------------------------*/
 function find_higest_saleing_product($limit){
   global $db;
   $sql  = "SELECT p.name, COUNT(s.product_id) AS totalSold, SUM(s.qty) AS totalQty";
   $sql .= " FROM sales s";
   $sql .= " LEFT JOIN products p ON p.id = s.product_id ";
   $sql .= " GROUP BY s.product_id";
   $sql .= " ORDER BY SUM(s.qty) DESC LIMIT ".$db->escape((int)$limit);
   return $db->query($sql);
 }

/*--------------------------------------------------------------*/
 /* Function for Find Highest selling Product
 /*--------------------------------------------------------------*/
 function find_highest_sales_by_city(){
   global $db;
   $sql  = "SELECT c.id,c.name,c.city, COUNT(s.customer_id) AS totalSold, SUM(s.qty) AS totalQty";
   $sql .= " FROM sales s";
   $sql .= " LEFT JOIN customers c ON c.id = s.customer_id ";
   $sql .= " GROUP BY s.customer_id";
   $sql .= " ORDER BY SUM(s.id) DESC";
   return $db->query($sql);
 }

 /*--------------------------------------------------------------*/
 /* Function for Find Sold Product
 /*--------------------------------------------------------------*/
 function find_sales_date(){
   global $db;
  $sql  = "SELECT c.date, COUNT(s.customer_id) AS totalSolds";
   $sql .= " FROM sales s";
   $sql .= " LEFT JOIN customers c ON c.id = s.customer_id ";
   $sql .= " GROUP BY s.customer_id";
  // $sql .= " ORDER BY SUM(s.id) DESC";
   return $db->query($sql);
 }
 

 /*--------------------------------------------------------------*/
 /* Function for Find Avail Products
 /*--------------------------------------------------------------*/
 function find_products_avail(){
   global $db;
  $sql  = "SELECT p.id,p.name,p.quantity";
  $sql .= " FROM products p";
  $sql .= " WHERE p.quantity < '100' ";
  $sql .= " ORDER BY p.quantity ASC";
  return find_by_sql($sql);
 }
 /*--------------------------------------------------------------*/
 /* Function for find all sales
 /*--------------------------------------------------------------*/
 function find_all_sale(){
   global $db;
   $sql  = "SELECT s.id,s.sales_invoice,s.qty,s.price,s.term,s.date,p.name,x.name AS salesman,c.name AS customer";
   $sql .= " FROM sales s";
   $sql .= " LEFT JOIN products p ON s.product_id = p.id";
  //  $sql .= " LEFT JOIN salesman l ON s.salesman_id = l.id";
   $sql  .=" LEFT JOIN employees x ON x.id = s.salesman_id";
   $sql  .=" LEFT JOIN customers c ON c.id = s.customer_id";
   $sql .= " ORDER BY s.date DESC";
   return find_by_sql($sql);
 }
  /*--------------------------------------------------------------*/
 /* Function for find all codes
 /*--------------------------------------------------------------*/
 function find_all_code(){
  global $db;
  $sql  = "SELECT c.id,c.name";
  $sql .= " FROM codes c";
  $sql .= " ORDER BY c.id DESC";
  return find_by_sql($sql);
}
/*--------------------------------------------------------------*/
 /* Function for find all sales
 /*--------------------------------------------------------------*/
 function find_all_unit(){
  global $db;
  $sql  = "SELECT u.id,u.name";
  $sql .= " FROM unit_measures u";
  $sql .= " ORDER BY u.id DESC";
  return find_by_sql($sql);
}
 /*--------------------------------------------------------------*/
 /* Function for find all sales
 /*--------------------------------------------------------------*/
 function find_all_customers(){
  global $db;
  $sql  = "SELECT c.id,c.codename,c.name,c.gender,c.balance,c.date_paid,c.contact,c.city";
  $sql .= " FROM customers c";
  $sql .= " ORDER BY c.id DESC";
  return find_by_sql($sql);
}
 /*--------------------------------------------------------------*/
 /* Function for Display Recent sale
 /*--------------------------------------------------------------*/
function find_recent_sale_added($limit){
  global $db;
  $sql  = "SELECT s.id,s.qty,s.price,s.date,p.name";
  $sql .= " FROM sales s";
  $sql .= " LEFT JOIN products p ON s.product_id = p.id";
  $sql .= " ORDER BY s.date DESC LIMIT ".$db->escape((int)$limit);
  return find_by_sql($sql);
}
/*--------------------------------------------------------------*/
/* Function for Generate sales report by two dates
/*--------------------------------------------------------------*/
function find_sale_by_dates($start_date,$end_date){
  global $db;
  $start_date  = date("Y-m-d", strtotime($start_date));
  $end_date    = date("Y-m-d", strtotime($end_date));
  $sql  = "SELECT s.date, p.name,p.sale_price,p.buy_price,";
  $sql .= "COUNT(s.product_id) AS total_records,";
  $sql .= "SUM(s.qty) AS total_sales,";
  $sql .= "SUM(p.sale_price * s.qty) AS total_saleing_price,";
  $sql .= "SUM(p.buy_price * s.qty) AS total_buying_price ";
  $sql .= "FROM sales s ";
  $sql .= "LEFT JOIN products p ON s.product_id = p.id";
  $sql .= " WHERE s.date BETWEEN '{$start_date}' AND '{$end_date}'";
  $sql .= " GROUP BY DATE(s.date),p.name";
  $sql .= " ORDER BY DATE(s.date) DESC";
  return $db->query($sql);
}
/*--------------------------------------------------------------*/
/* Function for Generate Daily sales report
/*--------------------------------------------------------------*/
function  dailySales($year,$month){
  global $db;
  $sql  = "SELECT s.qty,";
  $sql .= " DATE_FORMAT(s.date, '%Y-%m-%e') AS date,p.name,";
  $sql .= "SUM(p.sale_price * s.qty) AS total_saleing_price";
  $sql .= " FROM sales s";
  $sql .= " LEFT JOIN products p ON s.product_id = p.id";
  $sql .= " WHERE DATE_FORMAT(s.date, '%Y-%m' ) = '{$year}-{$month}'";
  $sql .= " GROUP BY DATE_FORMAT( s.date,  '%e' ),s.product_id";
  return find_by_sql($sql);
}
/*--------------------------------------------------------------*/
/* Function for Generate Monthly sales report
/*--------------------------------------------------------------*/
function  monthlySales($year){
  global $db;
  $sql  = "SELECT s.qty,";
  $sql .= " DATE_FORMAT(s.date, '%Y-%m-%e') AS date,p.name,";
  $sql .= "SUM(p.sale_price * s.qty) AS total_saleing_price";
  $sql .= " FROM sales s";
  $sql .= " LEFT JOIN products p ON s.product_id = p.id";
  $sql .= " WHERE DATE_FORMAT(s.date, '%Y' ) = '{$year}'";
  $sql .= " GROUP BY DATE_FORMAT( s.date,  '%c' ),s.product_id";
  $sql .= " ORDER BY date_format(s.date, '%c' ) DESC";
  return find_by_sql($sql);
}

?>
