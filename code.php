<?php
  $page_title = 'DBEI | Product codes';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
  
  $all_codes = find_prcode();
?>
<?php
 if(isset($_POST['add_code'])){
   $req_field = array('code-name');
   validate_fields($req_field);
      $code_name = remove_junk($db->escape($_POST['code-name']));
      $date = make_date();
   if(empty($errors)){
      $sql  = "INSERT INTO codes (name,date)";
      $sql .= " VALUES ('{$code_name}','{$date}')";

      if($db->query($sql)){
        $session->msg("s", "Code created successfully.");
        redirect('code.php',false);
      } else {
        $session->msg("d", "Sorry! failed to create.");
        redirect('code.php',false);
      }
   } else {
     $session->msg("d", $errors);
     redirect('code.php',false);
   }
 }
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
  <div class="row">
     <div class="col-md-3 pull-right noti">
       <?php echo display_msg($msg); ?>
    </div>
  </div>
   <div class="row">
     <ol class="breadcrumb pull-right">
        <li><a href="admin.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Product Codes</li>
    </ol>
   <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Product Codes</b></p>
    <div class="col-md-3">
      <div class="panel panel-default">
        <!-- <div class="panel-heading">
         <strong>
           <span class="glyphicon glyphicon-th"></span>
           
        </strong>
       </div> -->
        <div class="panel-body">
          <form method="post" action="code.php">
            <div class="form-group">
                <input type="text" class="form-control input-sm code" name="code-name" placeholder="New Product code" autofocus> 
            </div>
           
            <button type="submit" name="add_code" class="btn btn-danger btn-sm pull-right">Add Code</button>
        </form>
        </div>
      </div>
    </div>
    <div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-body">
          <!-- <div class="col-sm-4 pull-left input-group">
            <span><i class="glyphicon glyphicon-exclamation-sign"></i></span>
            <ul class="info-menu list-inline list-unstyled">
            <li class="">
              <a href="#" data-toggle="tooltip" title="Reminder: .">
                <i class="glyphicon glyphicon-exclamation-sign"></i>
              </a>
            </li>
          </ul>
          </div> -->
          <div class="col-sm-4 pull-right input-group">
            <input type="text" class="form-control input-sm" id="search" placeholder="Search product code here..">
              <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
          </div>
          <table class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th class="text-center" style="width: 50px;">#</th>
                    <th>Code Names</th>
                    <th>Date Added</th>
                    <th class="text-center" style="width: 100px;">Actions</th>
                </tr>
            </thead>
            <tbody class="tablesearch">
              <?php foreach ($all_codes as $code):?>
                <tr>
                    <td class="text-center"><?php echo count_id();?></td>
                    <td class="code"><?php echo remove_junk(ucfirst($code['name'])); ?></td>
                    <td class="code"><?php echo remove_junk(ucfirst($code['date'])); ?></td>
                    <td class="text-center">
                      <div class="btn-group">
                        <a href="edit_code.php?id=<?php echo (int)$code['id'];?>" data-toggle="tooltip" title="Edit">
                          &nbsp;&nbsp;<i class="glyphicon glyphicon-edit"></i>&nbsp;&nbsp;
                        </a>
                        
                        <!-- <a href="#id=<//?php echo (int)$codes['id'];?>"  class="btn btn-xs btn-danger" title="Remove" data-toggle="modal" data-target="#myModal">
                          <span class="glyphicon glyphicon-trash"></span>
                        </a> -->

                        <!-- <a href="delete_code.php?id=<//?php echo (int)$code['id'];?>"  class="btn btn-xs btn-danger" title="Remove" data-toggle="tooltip">
                        &nbsp;&nbsp;<i class="glyphicon glyphicon-trash"></i>&nbsp;&nbsp;
                        </a> -->
                      </div>
                    </td>

                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
       </div>
    </div>
    </div>
   </div>
   <!-- <//?php include_once('layouts/code_modal.php'); ?> -->
  </div>
  <?php include_once('layouts/footer.php'); ?>
  <?php include_once('includes/searchjs.php'); ?>

