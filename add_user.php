<?php
  $page_title = 'DBEI | Add User';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
  $groups = find_all('user_groups');
  $employees = find_all('employees');

?>
<?php
  if(isset($_POST['add_user'])){

   $req_fields = array('full-name','username','password','level' );
   validate_fields($req_fields);

   if(empty($errors)){
       $name   = (int)$db->escape($_POST['full-name']);
       $username   = remove_junk($db->escape($_POST['username']));
       $password   = remove_junk($db->escape($_POST['password']));
       $user_level = (int)$db->escape($_POST['level']);
       $password = sha1($password);
        $query = "INSERT INTO users (";
        $query .="employee_id,username,password,user_level,status";
        $query .=") VALUES (";
        $query .=" '{$name}', '{$username}', '{$password}', '{$user_level}','1'";
        $query .=")";
        if($db->query($query)){
          //sucess
          $session->msg('s',"User account created successfully.");
          redirect('users.php', false);
        } else {
          //failed
          $session->msg("d", "Sorry! failed to create a user.");
          redirect('add_user.php', false);
        }
   } else {
     $session->msg("d", $errors);
      redirect('add_user.php',false);
   }
 }
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
<div class="col-md-3 pull-right noti">
    <?php echo display_msg($msg); ?>
 </div>
  <div class="row"><br>
  <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Users</b></p>
    <div class="col-md-5">
      <div class="panel panel-default">
      <div class="panel-heading">
        <strong>
          <!-- <span class="glyphicon glyphicon-th"></span> -->
          <!-- <span>Add New User</span> -->
       </strong>
      </div>
      <div class="panel-body">
        <div class="col-md-10">
          <form method="post" action="add_user.php">
            <div class="form-group">
                <label for="name" class="text-muted">Employee Name</label>
                <!-- <input type="text" class="form-control initial_input" name="full-name" autofocus placeholder="Full Name"> -->
                <select class="form-control input-sm" name="full-name">
                  <?php foreach ($employees as $employee ):?>
                   <option value="<?php echo $employee['id'];?>"><?php echo ucwords($employee['name']);?></option>
                <?php endforeach;?>
                </select>
            </div>
            <div class="form-group">
                <label for="username" class="text-muted">Username</label>
                <input type="text" class="form-control input-sm initial_input" name="username" placeholder="Your username">
            </div>
            <div class="form-group">
                <label for="password" class="text-muted">Password</label>
                <input type="password" class="form-control input-sm" name ="password"  placeholder="Your password">
            </div>
            <div class="form-group">
              <label for="level" class="text-muted">User Role</label>
                <select class="form-control input-sm" name="level">
                  <?php foreach ($groups as $group ):?>
                   <option value="<?php echo $group['group_level'];?>"><?php echo ucwords($group['group_name']);?></option>
                <?php endforeach;?>
                </select>
            </div>
            <div class="form-group clearfix">
              <button type="button" name="cancel" class="btn btn-default btn-sm pull-left" onclick="goBack();">Cancel</button>
              <button type="submit" name="add_user" class="btn btn-danger btn-sm pull-right">Add User</button>
              
            </div>
        </form>
        </div>

      </div>

      </div>
  </div>
  </div>

<?php include_once('layouts/footer.php'); ?>
