<?php
  $page_title = 'DBEI | Employees';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
  
  $all_employees = find_employee();
?>
<?php
 if(isset($_POST['add_employee'])){
   $req_field = array('employee-name');
   validate_fields($req_field);
      $employee_name = remove_junk($db->escape($_POST['employee-name']));
      $age = remove_junk($db->escape($_POST['age']));
      $gender = remove_junk($db->escape($_POST['gender']));
      $date = make_date();

   if(empty($errors)){
      $sql  = "INSERT INTO employees (name,age,gender,date)";
      $sql .= " VALUES ('{$employee_name}','{$age}','{$gender}','{$date}')";

      if($db->query($sql)){
        $session->msg("s", "Employees created successfully.");
        redirect('employees.php',false);
      } else {
        $session->msg("d", "Sorry! failed to create.");
        redirect('employees.php',false);
      }
   } else {
     $session->msg("d", $errors);
     redirect('employees.php',false);
   }
 }
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
  <div class="row">
     <div class="col-md-3 pull-right noti">
       <?php echo display_msg($msg); ?>
    </div>
  </div>
   <div class="row">
     <ol class="breadcrumb pull-right">
        <li><a href="admin.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Employees</li>
    </ol>
   <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Employees</b></p>
    <div class="col-md-3">
      <div class="panel panel-default">
        <div class="panel-body">
          <form method="post" action="employees.php">
            <div class="form-group">
                <input type="text" class="form-control input-sm code" name="employee-name" placeholder="New Employee" autofocus> 
            </div>
            <div class="form-group col-sm-6">
                <input type="number" class="form-control input-sm" name="age" placeholder="Age"> 
            </div>
            <div class="form-group col-sm-6">
              <select class="form-control input-sm" name="gender">
                  <option disabled selected>Gender</option>
                  <option value="male">Male</option>
                  <option value="female">Female</option>
               </select>
            </div>
           
            <button type="submit" name="add_employee" class="btn btn-danger btn-sm pull-right">Add Employee</button>
        </form>
        </div>
      </div>
    </div>
    <div class="col-md-9">
    <div class="panel panel-default">
     
        <div class="panel-body">
          <div class="col-sm-4 pull-right input-group">
            <input type="text" class="form-control input-sm" id="search" placeholder="Search a employee here..">
              <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
          </div>
          <table class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th class="text-center" style="width: 50px;">#</th>
                    <th>Employee Names</th>
                    <th>Ages</th>
                    <th>Gender</th>
                    <th class="text-center" style="width: 100px;">Actions</th>
                </tr>
            </thead>
            <tbody class="tablesearch">
              <?php foreach ($all_employees as $employees):?>
                <tr>
                    <td class="text-center"><?php echo count_id();?></td>
                    <td><?php echo remove_junk(ucfirst($employees['name'])); ?></td>
                    <td><?php echo remove_junk(ucfirst($employees['age'])); ?></td>
                    <td><?php echo remove_junk(ucfirst($employees['gender'])); ?></td>
                    <td class="text-center">
                      <div class="btn-group">
                        <a href="edit_employees.php?id=<?php echo (int)$employees['id'];?>" data-toggle="tooltip" title="Edit">
                          &nbsp;&nbsp;<i class="glyphicon glyphicon-edit"></i>&nbsp;&nbsp;
                        </a>
                        
                        <!-- <a href="#id=<//?php echo (int)$codes['id'];?>"  class="btn btn-xs btn-danger" title="Remove" data-toggle="modal" data-target="#myModal">
                          <span class="glyphicon glyphicon-trash"></span>
                        </a> -->

                        <!-- <a href="delete_code.php?id=<//?php echo (int)$code['id'];?>"  class="btn btn-xs btn-danger" title="Remove" data-toggle="tooltip">
                        &nbsp;&nbsp;<i class="glyphicon glyphicon-trash"></i>&nbsp;&nbsp;
                        </a> -->
                      </div>
                    </td>

                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
       </div>
    </div>
    </div>
   </div>
   <?php include_once('layouts/code_modal.php'); ?>
  </div>
  <?php include_once('layouts/footer.php'); ?>
  <?php include_once('includes/searchjs.php'); ?>

