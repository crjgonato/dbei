<?php
  $page_title = 'DBEI | Expenses';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
   page_require_level(2);
  $products = join_product_table();
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
  <div class="row"><br>
    <ol class="breadcrumb pull-right">
        <li><a href="admin.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Route Expenses</li>
      </ol>
  <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Route Expenses</b></p>
  <div class="col-md-3 pull-right noti">
  <?php echo display_msg($msg); ?>
</div>
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
        <div class="col-sm-4 pull-left input-group">
            <input type="text" class="form-control input-sm" id="search" placeholder="Search route here.." autofocus>
              <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
          </div>
         <div class="pull-right">
           <a href="#" class="btn btn-danger input-xs">Add New</a>
         </div>
        </div>
        <div class="panel-body">
          <table class="table table-bordered table-condensed">
            <thead>
              <tr>
                <th class="text-center" style="width: 50px;">#</th>
                <!-- <th> Photo</th> -->
                <th> Date </th>
                <!-- <th> Units </th> -->
                <th> SI No. </th>
                <!-- <th class="text-center" style="width: 10%;"> Categories </th> -->
                <th class="text-center"> Expenses Account </th>
                <th class="text-center"> Expenses Description </th>
                <th> Units </th>
                <th class="text-center" style="width: 10%;"> Quantity </th>
                <th class="text-center" style="width: 10%;"> Unit Price </th>
                <th class="text-center" style="width: 10%;"> Amount </th>
                <th class="text-center" style="width: 100px;"> Actions </th>
              </tr>
            </thead>
            <!-- <tbody class="tablesearch">
              <?php foreach ($products as $product):?>
              <tr>
                <td class="text-center"><?php echo count_id();?></td>
                <td> <?php echo remove_junk($product['code']); ?></td>
                <td><a href=""> <?php echo remove_junk($product['name']); ?></a></td>
                <td class="text-center"> <?php echo remove_junk($product['categorie']); ?></td>
                <td class="text-center quantity"> <?php echo remove_junk($product['quantity']); ?></td>
                <td> <?php echo remove_junk($product['unit']); ?></td>
                <td class="text-center"> <?php echo remove_junk($product['buy_price']); ?></td>
                <td class="text-center"> <?php echo remove_junk($product['sale_price']); ?></td>
                <td class="text-center">
                  <div class="btn-group">
                    <a href="edit_product.php?id=<?php echo (int)$product['id'];?>" title="Edit" data-toggle="tooltip">
                      &nbsp;&nbsp;<i class="glyphicon glyphicon-edit"></i>&nbsp;&nbsp;
                    </a>
                  </div>
                </td>
              </tr>
             <?php endforeach; ?>
            </tbody> -->
          </tabel>
        </div>
      </div>
    </div>
    <?php include_once('layouts/product_modal.php'); ?>
  </div>
  <?php include_once('layouts/footer.php'); ?>
  <?php include_once('includes/searchjs.php'); ?>

 
  
  <script>
    $(document).ready(function() {
      // the following will select all 'td' elements with class "of_number_to_be_evaluated"
      // if the TD element has a '-', it will assign a 'red' class, and do the same for green.
      $(".quantity:contains('-')").addClass('red');
      //$("td:contains('+')").addClass('green');      
    });
  </script>
