<!-- <meta http-equiv="refresh" content="1;url=http://jdac.co/logout.php" /> -->
<?php
  $page_title = 'DBEI | Home Page';
  require_once('includes/load.php');
  if (!$session->isUserLoggedIn(true)) { redirect('index.php', false);}
?>
<?php include_once('layouts/header.php'); ?>
<div class="row">
<div class="col-md-3 pull-right noti">
<?php echo display_msg($msg); ?>
</div><br>
 <div class="col-md-12">
    <div class="panel">
      <div class="panel-body text-center">
         <h1>Welcome to DBEI</h1>
         <p>Explore and use the side menu.</p>
      </div> 
    </div>
 </div>
</div>
<?php include_once('layouts/footer.php'); ?>
