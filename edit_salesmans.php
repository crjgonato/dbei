<?php
  $page_title = 'DBEI | Edit Salesman';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
?>
<?php
  //Display all salesman.
  $man = find_by_id('salesman',(int)$_GET['id']);
  //$all_salesman = find_all('salesman');
  $all_employees = find_all('employees');
  if(!$man){
    $session->msg("d","Missing salesman id.");
    redirect('salesman.php');
  }
?>
<?php
if(isset($_POST['edit_salesman'])){
  $req_field = array('salesman','platenumber');
  validate_fields($req_fields);

  $sales_man = remove_junk($db->escape($_POST['salesman']));
  $platenumber = remove_junk($db->escape($_POST['platenumber']));
  $date = make_date();
  if(empty($errors)){
      $sql = "UPDATE salesman SET";
      $sql .= " employee_id='{$sales_man}', platenumber ='{$platenumber}', date_mod='{$date}'";
      $sql .= " WHERE id='{$man['id']}'";

     $result = $db->query($sql);
     if($result && $db->affected_rows() === 1) {
       $session->msg("s", "Salesman updated succesfully.");
       redirect('salesman.php',false);
     } else {
       $session->msg("d", "Sorry! failed to update.");
       redirect('edit_salesmans.php',false);
     }
  } else {
    $session->msg("d", $errors);
    redirect('salesman.php',false);
  }
}
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
  <div class="row">
     <div class="col-md-3 pull-right noti">
       <?php echo display_msg($msg); ?>
    </div>
  </div>
   <div class="row">
   <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Edit Salesman</b></p>
    <div class="col-md-3">
      <div class="panel panel-default">
        <div class="panel-body">
          <form method="post" action="edit_salesmans.php?id=<?php echo (int)$man['id'];?>">
            <div class="form-group">
            <p>Salesman<p>
                 <!-- <input type="text" class="form-control input-sm man code" name="salesman" value="<//?php echo remove_junk(ucfirst($man['name']));?>"> -->
                 <select class="form-control input-sm man" name="salesman">
                         <?php foreach ($all_employees as $employees): ?>
                              <option value="<?php echo (int)$employees['id']; ?>" <?php if($man['employee_id'] === $employees['id']): echo "selected"; endif; ?> >
                              <?php echo remove_junk($employees['name']); ?></option>
                        <?php endforeach; ?>
                    </select>
            </div>
            
             <div class="form-group">
             <p>Plate number<p>
               <input type="text" class="form-control input-sm man code" name="platenumber" value="<?php echo remove_junk(ucfirst($man['platenumber']));?>">
           </div>
            <button type="button" name="cancel" class="btn btn-default btn-sm pull-left" onclick="goBack();">Cancel</button>
            <button type="submit" name="edit_salesman" class="btn btn-danger btn-sm btnupdate pull-right" disabled>Apply Changes</button>
        </form>
        </div>
      </div>
    </div>
   </div>
   <?php include_once('layouts/code_modal.php'); ?>
  </div>
  <?php include_once('layouts/footer.php'); ?>
  <?php include_once('includes/searchjs.php'); ?>
<script>
  $().ready(function() {
    //disbaled update button after any changes above inputs
    //
    $(".man").focus( function(){
        $(".btnupdate").prop('disabled',false);
    });
  });
</script>
