     </div>
     <div class="footer">&copy; <?php echo date("Y"); ?> <b>Danica Basic Essentials Inc.</b>  | <a href="mailto:crjgonato@gmail.com" class="text-dark m-l-5">Support</a></div>
    </div>
    </div>
  <script type="text/javascript" src="jquery.min.js"></script>
  <script type="text/javascript" src="bootstrap.min.js"></script>   
  <script type="text/javascript" src="bootstrap-datepicker.min.js"></script>
  <script type="text/javascript" src="functions.js"></script>
  
  <!-- Modal -->
<div class="modal fade in" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">About</h5>
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> -->
      </div>
      <div class="modal-body">
        <p>DBEI - Stock Inventory Management System</p>
        <p>The latest updates has already been installed.</p>
        <p>Version 1.12.20</p><br>
        <div class="progress-widget">
            <div class="progress-data"><span class="name">Software Completion</span><span class="progress-value"> 70%</span></div>
            <div class="progress">
              <div style="width: 70%;" class="progress-bar progress-bar-danger progress-xs"></div>
            </div>
          </div>
        
      </div>
      <div class="modal-footer">
        <p class="pull-left">Made with love by <b>RJ GONATO</b></p>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function(event){
    $(function() {
      $('body').removeClass('fade-out');
    });
  });
  
  $(document).keydown(function(event){
      if(event.keyCode==123){
          return false;
      }
      else if (event.ctrlKey && event.shiftKey && event.keyCode==73){        
              return false;
      }
  });

  $(document).on("contextmenu",function(e){        
    e.preventDefault();
  });

  $('.alert').delay(5000).fadeOut(5000)


  var selector = '.navi li';
  $(selector).on('click', function(){
      $(selector).removeClass('active');
      $(this).addClass('active');
  });
</script>

  </body>
</html>

<?php if(isset($db)) { $db->db_disconnect(); } ?>
