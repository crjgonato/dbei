<?php 
$user = current_user();
?>
<!DOCTYPE html>
  <html oncontextmenu="return false;">
    <head>
    <meta charset="UTF-8">
    <title>
    <?php if (!empty($page_title))
           echo remove_junk($page_title);
            elseif(!empty($user))
           echo ucfirst($user['username']);
            else echo "DBEI - DANICA Basic Essentials, Inc";?>
    </title>
    <link rel="stylesheet" href="bootstrap.min.css"/>
    <link rel="stylesheet" href="datepicker3.min.css" />
    <link rel="stylesheet" href="main.css" />
    <!-- <link rel="stylesheet" href="libs/css/cdn.css"/> -->
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-102288567-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-102288567-1');
</script> -->

<!-- //Ads by Google -->
<!-- <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-5461731021513846",
    enable_page_level_ads: true
  });
</script> -->
<!-- // -->
<!-- <script>document.body.className += ' fade-out';</script> -->
  </head>
  <body class="fade-out">
  <?php if($session->isUserLoggedIn(true)): ?>
    <header id="header">
      <div class="logo pull-left" onclick="location.href = '';" style="cursor: pointer;">
       <!-- <i>The Stockbook</i></div> -->
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DBEI 
      </div>
      <div class="header-content">
        <div class="header-date pull-left">
          <!-- <strong><//?php echo make_date();?></strong> -->
          <!-- <a class="pull-left" href="" title="force to reload page"><span class="glyphicon glyphicon-fullscreen"></span></a>&nbsp;&nbsp; -->
          <p class="text-muted pull-right clearfix title_name"><b>DANICA Basic Essentials, Inc.</b></p>
        </div>
        <div class="pull-right clearfix">
          <ul class="info-menu list-inline list-unstyled">
            <li class="profile">
              <a href="#" data-toggle="dropdown" class="toggle" aria-expanded="false">
                <!-- <img src="./uploads/users/<//?php echo $user['image'];?>" alt="" class="img-circle img-inline"> -->
                <span> <i class="glyphicon glyphicon-user"></i>&nbsp; <?php echo remove_junk(ucfirst($user['username'])); ?>&nbsp;&nbsp;<i class="caret"></i></span>
              </a>
              <ul class="dropdown-menu">
              <!-- <li>
                    <a href="profile.php?id=<//?php echo (int)$user['id'];?>">
                        <i class="glyphicon glyphicon-cog"></i>
                        Preference
                    </a>
                </li> -->
              <li>
                  <a href="settings.php" title="Settings">
                      <!-- <i class="glyphicon glyphicon-cog"></i> -->
                     <i class="glyphicon glyphicon-cog"></i> Settings
                  </a>
              </li>
              <li>
                  <a href="system.php" title="Updates">
                      <!-- <i class="glyphicon glyphicon-cog"></i> -->
                      <i class="glyphicon glyphicon-cloud-download"></i> System Updates 
                  </a>
              </li>
               <li>
                  <a href="#" title="About" data-toggle="modal" data-target="#exampleModal">
                      <!-- <i class="glyphicon glyphicon-cog"></i> -->
                      <i class="glyphicon glyphicon-console"></i> About 
                  </a>
              </li>
              <li class="last btn-danger">
                  <a href="logout.php">
                      <i class="glyphicon glyphicon-log-out"></i> Logout
                  </a>
              </li>
            </ul>
            </li>
          </ul>
        </div>
        <!-- <div class=" clearfix">
          <ul class="info-menu list-inline list-unstyled">
            <li class="profile">
              <a href="#" data-toggle="dropdown" class="toggle" aria-expanded="false">
                <span><i class="glyphicon glyphicon-globe"></i> <span class="badge">1</span> </span>
              </a>
              <ul class="dropdown-menu">
              <li>
                 <a href="#" title="">
                      The Mobile Application for our Stock Inventory System is Coming Soon..
                  </a>
              </li>             
            </ul>
            </li>
          </ul>
        </div> -->
     </div>
    </header>
    <div class="sidebar">
      <?php if($user['user_level'] === '1'): ?>
        <!-- admin menu -->
        <?php include_once('admin_menu.php');?>

      <?php elseif($user['user_level'] === '2'): ?>
        <!-- Special user -->
        <?php include_once('special_menu.php');?>

      <?php elseif($user['user_level'] === '3'): ?>
        <!-- Routes user -->
        <?php include_once('routes_menu.php');?>

      <?php elseif($user['user_level'] === '4'): ?>
        <!-- User menu -->
        <?php include_once('routes_menu.php');?>
        
      <?php endif;?>
   </div>
<?php endif;?>
<div class="page">
  <div class="container-fluid">
<?php include_once('history.php'); ?>
