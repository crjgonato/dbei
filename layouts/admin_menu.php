<ul class="navi">
  <li>
    <a href="admin.php">
      <i class="glyphicon glyphicon-home"></i>
      <span>Dashboard</span>
    </a>
  </li>
  <!-- <li>
    <a href="media.php" >
      <i class="glyphicon glyphicon-picture"></i>
      <span>Files</span>
    </a>
  </li>
  <li> -->
  <li>
    <a href="code.php" class="submenu-toggle">
      <i class="glyphicon glyphicon-barcode"></i>
      <span>Product Codes</span>
    </a>
    <!-- <ul class="nav submenu">
      
       <li><a href="p. roduct_code.php">Manage codes</a> </li>
       <li><a href="add_code.php">Add code</a> </li>
   </ul> -->
  </li> 
  <li>
  <li >
    <a href="measurements.php">
      <i class="glyphicon glyphicon-filter"></i>
      <span>Unit of Measures</span>
    </a>
  </li>
  <li>
    <a href="categorie.php">
      <i class="glyphicon glyphicon-folder-open"></i>
      <span>Categories</span>
    </a>
  </li>
  <li>
    <a href="#" class="submenu-toggle">
      <i class="glyphicon glyphicon-road"></i>
      <span>Routes</span>
      <!-- <i class="glyphicon glyphicon glyphicon-menu-down pull-right"></i> -->
    </a>
    <ul class="nav submenu">
      <li><a href="route.php">Route List</a> </li>
       <li><a href="#">Returns</a> </li>
      <li><a href="salesman.php">Salesman</a> </li>
    </ul>
  </li>
  <li>
        <a href="#" class="submenu-toggle">
        <i class="glyphicon glyphicon-list-alt"></i>
          <span>Expenses</span>
        </a>
          <ul class="nav submenu">
            <li><a href="expenses_route.php">Manage Expenses</a> </li>
            <li><a href="#">Add Expenses</a> </li>
            <li><a href="expenses_acct.php">Expense Accounts</a> </li>
            <li><a href="expenses_desc.php">Expenses Descriptions</a> </li>
          </ul>
      </li>
  <li>
    <a href="supplier.php">
      <i class="glyphicon glyphicon-phone-alt"></i>
      <span>Supplier</span>
    </a>
  </li>
  <li>
    <a href="#" class="submenu-toggle">
      <i class="glyphicon glyphicon-shopping-cart"></i>
      <span>Purchases</span>
      <!-- <i class="glyphicon glyphicon glyphicon-menu-down pull-right"></i> -->
    </a>
    <ul class="nav submenu">
      <li><a href="purchases.php">Manage purchases</a> </li>
      <li><a href="#">Add purchases</a> </li>
    </ul>
  </li>
  <li>
    <a href="#" class="submenu-toggle">
      <i class="glyphicon glyphicon-stats"></i>
      <span>Products</span>
      <!-- <i class="glyphicon glyphicon glyphicon-menu-down pull-right"></i> -->
    </a>
    <ul class="nav submenu">
       <li><a href="product.php">Manage products</a> </li>
       <li><a href="add_product.php">Add product</a> </li>
   </ul>
  </li>
  <li>
    <a href="#" class="submenu-toggle">
      <i class="glyphicon glyphicon-tasks"></i>
       <span>Sales</span>
       <!-- <i class="glyphicon glyphicon glyphicon-menu-down pull-right"></i> -->
      </a>
      <ul class="nav submenu">
         <li><a href="sales.php">Manage Sales</a></li>
         <li><a href="add_sales.php">Add Sale</a></li>
         <li><a href="#">Stock Issuances</a> </li>
         <!-- <li><a href="#">Stock Returns</a> </li> -->
         <li><a href="#">Accounts Receivable</a> </li>
         <!-- <li><a href="add_sale.php">Add Sale</a> </li> -->
     </ul>
  </li>
  <li>
    <a href="customers.php" >
      <i class="glyphicon glyphicon-credit-card"></i>
      <span>Customers</span>
    </a>
   
  </li>
  <li>
    <a href="channels.php" >
      <i class="glyphicon glyphicon-flag"></i>
      <span>Channels</span>
    </a>
   
  </li>
  
  <li>
    <a href="#" class="submenu-toggle">
      <i class="glyphicon glyphicon-signal"></i>
       <span>Sales Report</span>
       <!-- <i class="glyphicon glyphicon glyphicon-menu-down pull-right"></i> -->
      </a>
      <ul class="nav submenu">
        <li><a href="sales_report.php">Sales by date </a></li>
        <li><a href="monthly_sales.php">Monthly sales</a></li>
        <li><a href="daily_sales.php">Daily sales</a> </li>
      </ul>
  </li>
  <li>
    <a href="#" class="submenu-toggle">
      <i class="glyphicon glyphicon-user"></i>
      <span>Users</span>
      <!-- <i class="glyphicon glyphicon glyphicon-menu-down pull-right "></i> -->
    </a>
    <ul class="nav submenu">
      <li><a href="group.php">Manage Groups</a> </li>
      <li><a href="users.php">Manage Users</a> </li>
      <li><a href="logs.php">Logs</a> </li>
   </ul>
  </li>
  <li>
    <a href="employees.php">
      <i class="glyphicon glyphicon-link"></i>
      <span>Employees</span>
    </a>
  </li>
  
</ul>
