<?php 
$user = current_user();
?>
<!DOCTYPE html>
  <html oncontextmenu="return false;">
    <head>
    <meta charset="UTF-8">
    <title>
    <?php if (!empty($page_title))
           echo remove_junk($page_title);
            elseif(!empty($user))
           echo ucfirst($user['username']);
            else echo "DBEI - Danica Basic Essentials Inc";?>
    </title>
    <link rel="stylesheet" href="bootstrap.min.css"/>
    <link rel="stylesheet" href="datepicker3.min.css" />
    <link rel="stylesheet" href="main.css" />
    <!-- <link rel="stylesheet" href="libs/css/cdn.css"/> -->
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-102288567-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-102288567-1');
</script> -->

<!-- //Ads by Google -->
<!-- <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-5461731021513846",
    enable_page_level_ads: true
  });
</script> -->
<!-- // -->
  </head>
  <body>
  <?php if($session->isUserLoggedIn(true)): ?>
    <header id="header">
      <div class="logo pull-left" onclick="location.href = '';" style="cursor: pointer;">
       <!-- <i>The Stockbook</i></div> -->
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DBEI 
      </div>
      <div class="header-content">
        <div class="header-date pull-left">
          <!-- <strong><//?php echo make_date();?></strong> -->
          <!-- <a href="" title="force to reload page"><span class="glyphicon glyphicon-refresh"></span></a> -->
          <!-- <p class="text-muted pull-left clearfix title_name"><b>Danica Basic Essentials Inc.</b></p> -->
        </div>
     </div>
    </header>
<?php endif;?>
<div class="page_report">
  <div class="container-fluid">
<?php include_once('history.php'); ?>
