<ul class="navi">
  <li>
    <a href="routes.php">
      <i class="glyphicon glyphicon-home"></i>
      <span>Dashboard</span>
    </a>
  </li>
  <li>
    <a href="#" class="submenu-toggle">
      <i class="glyphicon glyphicon-road"></i>
       <span>My Routes</span>
      </a>
      <ul class="nav submenu">
         <li><a href="sales.php">Manage Sales</a> </li>
          <?php if($user['user_level'] === '1'): ?>
            <?php echo ('<li><a href="add_sale.php">Add Sale</a> </li>');?>
          <?php endif;?>
         
     </ul>
  </li>
  <li>
    <a href="#" class="submenu-toggle">
      <i class="glyphicon glyphicon-tasks"></i>
       <span>My Sales</span>
      </a>
      <ul class="nav submenu">
         <li><a href="sales.php">Manage Sales</a> </li>
         <li><a href="add_sale.php">Add Sale</a> </li>
     </ul>
  </li>
</ul>
