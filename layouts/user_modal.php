<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">You want to delete a User?</h4>
      </div>
      <!-- <div class="modal-body">
        ...
      </div> -->
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button> -->
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
        <a href="#" class="btn btn-secondary pull-left" data-dismiss="modal"><b>No</b></a>
        <a href="delete_user.php?id=<?php echo (int)$a_user['id'];?>"  class="btn btn-secondary"><b>Yes, I want to delete</b></a>
      </div>
    </div>
  </div