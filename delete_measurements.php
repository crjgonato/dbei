<?php
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
?>
<?php
  $units = find_by_id('unit_measures',(int)$_GET['id']);
  if(!$units){
    $session->msg("d","Missing Code id.");
    redirect('measurements.php');
  }
?>
<?php
  $delete_id = delete_by_id('unit_measures',(int)$units['id']);
  if($delete_id){
      $session->msg("s","Unit has been deleted.");
      redirect('measurements.php');
  } else {
      $session->msg("d","Unit deletion failed.");
      redirect('measurements.php');
  }
?>
