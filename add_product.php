<?php
  $page_title = 'DBEI | Add Product';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(2);
  $all_categories = find_all('categories');
  $all_codes = find_all('codes');
  $all_units = find_all('unit_measures');
  $all_photo = find_all('media');
?>
<?php
 if(isset($_POST['add_product'])){
   $req_fields = array('product-title','product-categorie','product-codes','product-units','product-quantity','buying-price', 'saleing-price' );
   validate_fields($req_fields);
   if(empty($errors)){
     $p_name  = remove_junk($db->escape($_POST['product-title']));
     $p_cat   = remove_junk($db->escape($_POST['product-categorie']));
     $p_codes   = remove_junk($db->escape($_POST['product-codes']));
     $p_units   = remove_junk($db->escape($_POST['product-units']));
     $p_qty   = remove_junk($db->escape($_POST['product-quantity']));
     $p_buy   = remove_junk($db->escape($_POST['buying-price']));
     $p_sale  = remove_junk($db->escape($_POST['saleing-price']));
     if (is_null($_POST['product-photo']) || $_POST['product-photo'] === "") {
       $media_id = '0';
     } else {
       $media_id = remove_junk($db->escape($_POST['product-photo']));
     }
     $date    = make_date();
     $query  = "INSERT INTO products (";
     $query .=" name,quantity,buy_price,sale_price,categorie_id,code_id,unit_id,media_id,date";
     $query .=") VALUES (";
     $query .=" '{$p_name}', '{$p_qty}', '{$p_buy}', '{$p_sale}', '{$p_cat}', '{$p_codes}', '{$p_units}', '{$media_id}', '{$date}'";
     $query .=")";
     $query .=" ON DUPLICATE KEY UPDATE name='{$p_name}'";

     
     
     if($db->query($query)){
       $session->msg('s',"Product created successfully. ");
       redirect('product.php', false);
     } else {
      $session->msg("d", "Sorry! failed to create a product");
       redirect('product.php', false);
     }

   } else{
     $session->msg("d", $errors);
     redirect('add_product.php',false);
   }

 }

?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
<div class="row">
<div class="col-md-3 pull-right noti">
<?php echo display_msg($msg); ?>
</div>
</div>
  <div class="row">
    <ol class="breadcrumb pull-right">
        <li><a href="admin.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Add Product</li>
      </ol>
  <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Add Product</b></p>
  <div class="col-md-6">
      <div class="panel panel-default">
        <div class="panel-body">
         <div class="col-md-12">
          <form method="post" action="add_product.php" class="clearfix">
              <div class="form-group">
                  <!-- <span class="input-group-addon">
                   <i class="glyphicon glyphicon-th-large"></i>
                  </span> -->
                  <p>Product Name</p>
                  <input type="text" class="form-control input-sm initial_input" name="product-title" autofocus placeholder="New Product">
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <p>Product Category</p>
                    <select class="form-control input-sm" name="product-categorie">
                      <option disabled selected>Select Category</option>
                    <?php  foreach ($all_categories as $cat): ?>
                      <option value="<?php echo (int)$cat['id'] ?>">
                        <?php echo $cat['name'] ?></option>
                    <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="col-md-6">
                    <p>Product Code</p>
                    <select class="form-control input-sm" name="product-codes">
                      <option disabled selected>Select Code</option>
                    <?php  foreach ($all_codes as $codes): ?>
                      <option value="<?php echo $codes['id'] ?>">
                        <?php echo $codes['name'] ?></option>
                    <?php endforeach; ?>
                    </select>
                  </div>
                  <!-- <div class="col-md-6">
                    <select class="form-control" name="product-photo">
                      <option value="">Select Product Photo</option>
                    <//?php  foreach ($all_photo as $photo): ?>
                      <option value="<//?php echo (int)$photo['id'] ?>">
                        <//?php echo $photo['file_name'] ?></option>
                    <//?php endforeach; ?>
                    </select>
                  </div> -->
                  
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                <div class="col-md-6">
                   
                     <!-- <span class="input-group-addon">
                      <i class="glyphicon glyphicon-shopping-cart"></i>
                     </span> -->
                     <p>Product Quantity</p>
                     <input type="text" class="form-control input-sm" name="product-quantity" placeholder="Quantity">
                  
                 </div>

                 <div class="col-md-6">
                   <p>Unit of Measurements</p>
                 <select class="form-control input-sm" name="product-units">
                   <option disabled selected>Select units</option>
                 <?php  foreach ($all_units as $units): ?>
                   <option value="<?php echo $units['id'] ?>">
                     <?php echo $units['name'] ?></option>
                 <?php endforeach; ?>
                 </select>
               </div>

                </div>
              </div>

              <div class="form-group">
               <div class="row">
                 
                 <div class="col-md-6">
                   <p>Buying Price</p>
                   <div class="input-group">
                     <span class="input-group-addon">
                       P
                     </span>
                     <input type="text" class="form-control input-sm" name="buying-price" placeholder="Price">
                     <!-- <span class="input-group-addon">.00</span> -->
                  </div>
                 </div>
                  <div class="col-md-6">
                    <p>Selling Price</p>
                    <div class="input-group">
                      <span class="input-group-addon">
                        P
                      </span>
                      <input type="text" class="form-control input-sm" name="saleing-price" placeholder="Price">
                      <!-- <span class="input-group-addon">.00</span> -->
                   </div>
                  </div>
               </div>
              </div>
              <button type="submit" name="add_product" class="btn btn-danger btn-sm pull-right">Add Product</button>
          </form>
         </div>
        </div>
      </div>
    </div>
  </div>

<?php include_once('layouts/footer.php'); ?>
