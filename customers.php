<?php
  $page_title = 'DBEI | Customers';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
  
  //$all_customers = find_all('customers');
  $all_channels = find_all('channels');
  $customers = join_customers_table();
?>
<?php
 if(isset($_POST['add_customer'])){
  //  $req_field = array('name','gender','balance','date_paid','contact','address');
  $req_field = array('codename','businessname','name','gender','channels','balance','contact','zipcode','street','brgy','city');
   validate_fields($req_field);
        $codename = remove_junk($db->escape($_POST['codename']));
        $businessname = remove_junk($db->escape($_POST['businessname']));
        $name = remove_junk($db->escape($_POST['name']));
        $gender = remove_junk($db->escape($_POST['gender']));
        $channels = remove_junk($db->escape($_POST['channels']));
        $balance = remove_junk($db->escape($_POST['balance']));
        $contact = remove_junk($db->escape($_POST['contact']));
        $zipcode = remove_junk($db->escape($_POST['zipcode']));
        $street = remove_junk($db->escape($_POST['street']));
        $brgy = remove_junk($db->escape($_POST['brgy']));
        $city = remove_junk($db->escape($_POST['city']));
        $date = make_date();

   if(empty($errors)){
        $sql  = "INSERT INTO customers (";
        $sql .="codename,businessname,name,gender,channels_id,balance,contact,zipcode,street,brgy,city,date";
        $sql .=") VALUES (";
        $sql .="'{$codename}','{$businessname}','{$name}','{$gender}','{$channels}','{$balance}','{$contact}','{$zipcode}','{$street}','{$brgy}','{$city}','{$date}'";
        $sql .=")";
        
      if($db->query($sql)){
        $session->msg("s", "Customer created successfully.");
        redirect('customers.php',false);
      } else {
        $session->msg("d", "Sorry! failed to create.");
        redirect('customers.php',false);
      }
   } else {
     $session->msg("d", $errors);
     redirect('customers.php',false);
   }
 }
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
  <div class="row">
  <div class="col-md-3 pull-right noti">
  <?php echo display_msg($msg); ?>
</div>
  </div>
   <div class="row">
     <ol class="breadcrumb pull-right">
        <li><a href="admin.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Customers</li>
      </ol>
   <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Customers</b></p>
    <div class="col-md-3">
      <div class="panel panel-default">
        <div class="panel-body">
          <form method="post" action="customers.php">
          <div class="form-group">
                <input type="text" class="form-control input-sm" name="codename" placeholder="New Customer Code" autofocus require="required"> 
            </div>
             <div class="form-group">
                <input type="text" class="form-control input-sm" name="businessname" placeholder="Business Name" autofocus require="required"> 
            </div>

            <div class="form-group">
                <input type="text" class="form-control input-sm" name="name" placeholder="Name" autofocus require="required"> 
            </div>
            
           <div class="form-group ">
              <div class="form-group col-sm-5">
                    <select class="form-control input-sm" name="gender">
                        <option disabled selected>Gender</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                </div>
                <div class="form-group col-sm-7">
                <select class="form-control input-sm" name="channels">
                        <option disabled selected>Channels</option>
                        <?php  foreach ($all_channels as $channels): ?>
                          <option value="<?php echo $channels['id'] ?>">
                            <?php echo $channels['name'] ?></option>
                        <?php endforeach; ?>
                        
                    </select> 
                </div>
           </div>
            <div class="form-group">
                <input type="text" class="form-control input-sm" name="contact" placeholder="Contact Number" autofocus require="required"> 
            </div>
            <div class="form-group">
                <input type="text" class="form-control input-sm" name="balance" placeholder="Payment Balance" autofocus require="required"> 
            </div>
           <fieldset>
           <!-- <legend><h5>Address</h5></legend> -->
           <div class="form-group">
                    <input type="text" class="form-control input-sm" name="zipcode" placeholder="ZipCode" autofocus require="required"> 
                    <!-- <textarea class="form-control" name="address" placeholder="Address"></textarea> -->
                </div>
              <div class="form-group">
                    <input type="text" class="form-control input-sm" name="street" placeholder="Street" autofocus require="required"> 
                    <!-- <textarea class="form-control" name="address" placeholder="Address"></textarea> -->
                </div>
                <div class="form-group">
                    <input type="text" class="form-control input-sm" name="brgy" placeholder="Barangay" autofocus require="required"> 
                    <!-- <textarea class="form-control" name="address" placeholder="Address"></textarea> -->
                </div>
                <div class="form-group">
                    <input type="text" class="form-control input-sm" name="city" placeholder="City" autofocus require="required"> 
                    <!-- <textarea class="form-control" name="address" placeholder="Address"></textarea> -->
                </div>
           </fieldset>
            <button type="submit" name="add_customer" class="btn btn-danger btn-sm pull-right">Add Customer</button>
        </form>
        </div>
      </div>
    </div>
    <div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-body">
        <div class="col-sm-4 pull-right input-group">
            <input type="text" class="form-control input-sm" id="search" placeholder="Search customer here..">
              <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
          </div>
          <table class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th class="text-center" style="width: 50px;">#</th>
                    <th>Codes</th>
                    <th>Business Name</th>
                    <th>Customer Name</th>
                    
                    <th>Balance</th>
                    <th>ZipCode</th>
                    <!-- <th>Contacts</th> -->
                    <th>City</th>
                    <th>Channels</th>
                    <th class="text-center" style="width: 100px;">Actions</th>
                </tr>
            </thead>
            <tbody class="tablesearch">
              <?php foreach ($customers as $customer):?>
              <tr>
                  <td class="text-center"><?php echo count_id();?></td>
                  <td><?php echo remove_junk(ucfirst($customer['codename'])); ?></td>
                  <td><?php echo remove_junk(ucfirst($customer['businessname'])); ?></td>
                  <td><a href=""><?php echo remove_junk(ucfirst($customer['name'])); ?></a></td>
                  <!-- <td><//?php echo remove_junk(ucfirst($customers['gender'])); ?></td> -->
                  <td><?php echo remove_junk(ucfirst($customer['balance'])); ?></td>
                  <!-- <td><?php echo remove_junk(ucfirst($customer['date_paid'])); ?></td> -->
                  <td><?php echo remove_junk(ucfirst($customer['zipcode'])); ?></td>
                  <td><?php echo remove_junk(ucfirst($customer['city'])); ?></td>
                  <td><?php echo remove_junk(ucfirst($customer['channels'])); ?></td>
                  <td class="text-center">
                    <div class="btn-group">
                      <!-- <a href="edit_customer.php?id=<//?php echo (int)$customers['id'];?>"  class="btn btn-xs btn-warning" data-toggle="tooltip" title="Edit"> -->
                      <!-- <a href="#"  class="btn btn-xs btn-warning" data-toggle="tooltip" title="Edit">
                      &nbsp;&nbsp;<i class="glyphicon glyphicon-edit"></i>&nbsp;&nbsp;
                      </a> -->
                      <a href="edit_customer.php?id=<?php echo (int)$customer['id'];?>" title="Edit" data-toggle="tooltip">
                      &nbsp;&nbsp;<i class="glyphicon glyphicon-edit"></i>&nbsp;&nbsp;
                    </a>
                      <!-- <a href="#?id=<//?php echo (int)$customers['id'];?>"  class="btn btn-xs btn-danger" title="Remove" data-toggle="tooltip">
                      &nbsp;&nbsp;<i class="glyphicon glyphicon-trash"></i>&nbsp;&nbsp;
                      </a> -->
                    </div>
                  </td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
       </div>
    </div>
    </div>
   </div>
   <!-- <//?php include_once('layouts/customer_modal.php'); ?> -->
  </div>
  <?php include_once('layouts/footer.php'); ?>
  <?php include_once('includes/searchjs.php'); ?>
