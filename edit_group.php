<?php
  $page_title = 'DBEI | Edit Group';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
   page_require_level(1);
?>
<?php
  $e_group = find_by_id('user_groups',(int)$_GET['id']);
  if(!$e_group){
    $session->msg("d","Missing Group id.");
    redirect('group.php');
  }
?>
<?php
  if(isset($_POST['update'])){

   $req_fields = array('group-name','group-level');
   validate_fields($req_fields);
   if(empty($errors)){
      $name = remove_junk($db->escape($_POST['group-name']));
      $level = remove_junk($db->escape($_POST['group-level']));
      $status = remove_junk($db->escape($_POST['status']));
      $date = make_date();

        $query  = "UPDATE user_groups SET ";
        $query .= "group_name='{$name}',group_level='{$level}',group_status='{$status}', date_mod='{$date}'";
        $query .= "WHERE ID='{$db->escape($e_group['id'])}'";
        $result = $db->query($query);
         if($result && $db->affected_rows() === 1){
          //sucess
          $session->msg('s',"Group updated  succesfully. ");
          redirect('group.php?id='.(int)$e_group['id'], false);
        } else {
          //failed
          $session->msg("d", "Sorry! failed to update.");
          redirect('edit_group.php?id='.(int)$e_group['id'], false);
        }
   } else {
     $session->msg("d", $errors);
    redirect('edit_group.php?id='.(int)$e_group['id'], false);
   }
 }
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
<div class="row"><br>
<p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Edit Group</b></p>
 <div class="col-md-7">
   
    <!-- <div class="text-center">
       <h3>Add new user Group</h3>
     </div> -->

     <div class="col-md-3 pull-right noti">
        <?php echo display_msg($msg); ?>
    </div>
    <div class="col-md-8">
     <div class="panel panel-default">
       <div class="panel-heading">
    <div class="panel-body">
      <div class="col-md-9">
         <form method="post" action="edit_group.php?id=<?php echo (int)$e_group['id'];?>" class="clearfix">
        <div class="form-group">
              <label for="name" class="control-label">Group Name</label>
              <input type="name" class="form-control input-sm name" name="group-name" value="<?php echo remove_junk(ucwords($e_group['group_name'])); ?>">
        </div>
        <div class="form-group">
              <label for="level" class="control-label">Group Level</label>
              <input type="number" class="form-control input-sm name" name="group-level" value="<?php echo (int)$e_group['group_level']; ?>" style="cursor:not-allowed;" readonly>
        </div>
        <div class="form-group">
          <label for="status">Status</label>
            <select class="form-control input-sm name" name="status">
                <option <?php if($e_group['group_status'] === '1') echo 'selected="selected"';?> value="1"> Active </option>
                <option <?php if($e_group['group_status'] === '0') echo 'selected="selected"';?> value="0">Deactive</option>
              </select>
        </div>
        <div class="form-group clearfix">
          <button type="button" name="cancel" class="btn btn-default btn-sm pull-left" onclick="goBack();">Cancel</button>
          <button type="submit" name="update" class="btn btn-danger pull-lef btnupdate btn-sm pull-right" disabled>Apply Changes</button>  
        </div>
      </form>
</div>


</div>
</div>
</div>
</div>
</div>
</div>

<?php include_once('layouts/footer.php'); ?>
<script>
  $().ready(function() {
    
    //disbaled update button after any changes above inputs
    //
    $(".name").click( function(){
        $(".btnupdate").prop('disabled',false);
    });
    

  });
</script>
