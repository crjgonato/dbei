<?php
  $page_title = 'DBEI | Expense Account';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
  
  $all_exp_account = find_exp_account();
?>
<?php
 if(isset($_POST['add_expaccount'])){
   $req_field = array('expaccount-name');
   validate_fields($req_field);
      $expaccount_name = remove_junk($db->escape($_POST['expaccount-name']));
      $date = make_date();

   if(empty($errors)){
      $sql  = "INSERT INTO expenses_account (name,date)";
      $sql .= " VALUES ('{$expaccount_name}','{$date}')";

      if($db->query($sql)){
        $session->msg("s", "Account created successfully.");
        redirect('expenses_acct.php',false);
      } else {
        $session->msg("d", "Sorry! failed to create.");
        redirect('expenses_acct.php',false);
      }
   } else {
     $session->msg("d", $errors);
     redirect('expenses_acct.php',false);
   }
 }
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
  <div class="row">
     <div class="col-md-3 pull-right noti">
       <?php echo display_msg($msg); ?>
    </div>
  </div>
   <div class="row">
     <ol class="breadcrumb pull-right">
        <li><a href="admin.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Expense Accounts</li>
    </ol>
   <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Expense Accounts</b></p>
    <div class="col-md-3">
      <div class="panel panel-default">
        <div class="panel-body">
          <form method="post" action="expenses_acct.php">
            <div class="form-group">
                <input type="text" class="form-control input-sm" name="expaccount-name" placeholder="New Expense Account" autofocus> 
            </div>
           
            <button type="submit" name="add_expaccount" class="btn btn-danger btn-sm pull-right">Add Account</button>
        </form>
        </div>
      </div>
    </div>
    <div class="col-md-9">
    <div class="panel panel-default">
     
        <div class="panel-body">
          <div class="col-sm-5 pull-right input-group">
            <input type="text" class="form-control input-sm" id="search" placeholder="Search a expense accounts here..">
              <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
          </div>
          <table class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th class="text-center" style="width: 50px;">#</th>
                    <th>Expense Accounts</th>
                    <th>Date Added</th>
                    <th class="text-center" style="width: 100px;">Actions</th>
                </tr>
            </thead>
            <tbody class="tablesearch">
              <?php foreach ($all_exp_account as $exp_account):?>
                <tr>
                    <td class="text-center"><?php echo count_id();?></td>
                    <td class=""><?php echo remove_junk(ucfirst($exp_account['name'])); ?></td>
                    <td class=""><?php echo remove_junk(ucfirst($exp_account['date'])); ?></td>
                    <td class="text-center">
                      <div class="btn-group">
                        <a href="edit_expaccount.php?id=<?php echo (int)$exp_account['id'];?>" data-toggle="tooltip" title="Edit">
                          &nbsp;&nbsp;<i class="glyphicon glyphicon-edit"></i>&nbsp;&nbsp;
                        </a>
                        
                        <!-- <a href="#id=<//?php echo (int)$codes['id'];?>"  class="btn btn-xs btn-danger" title="Remove" data-toggle="modal" data-target="#myModal">
                          <span class="glyphicon glyphicon-trash"></span>
                        </a> -->

                        <!-- <a href="delete_code.php?id=<//?php echo (int)$code['id'];?>"  class="btn btn-xs btn-danger" title="Remove" data-toggle="tooltip">
                        &nbsp;&nbsp;<i class="glyphicon glyphicon-trash"></i>&nbsp;&nbsp;
                        </a> -->
                      </div>
                    </td>

                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
       </div>
    </div>
    </div>
   </div>
   <?php include_once('layouts/code_modal.php'); ?>
  </div>
  <?php include_once('layouts/footer.php'); ?>
  <?php include_once('includes/searchjs.php'); ?>

