<?php
  $page_title = 'DBEI | Edit channels';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
  
  //$all_channels = find_all('channels');
?>
<?php
  //Display all catgories.
  $channels = find_by_id('channels',(int)$_GET['id']);
  if(!$channels){
    $session->msg("d","Missing channels id.");
    redirect('channels.php');
  }
?>
<?php
if(isset($_POST['edit_channels'])){
  $req_field = array('channels-name');
  validate_fields($req_field);
  $channels_name = remove_junk($db->escape($_POST['channels-name']));
  $date = make_date();
  if(empty($errors)){
        $sql = "UPDATE channels SET name='{$channels_name}', date_mod='{$date}'";
       $sql .= " WHERE id='{$channels['id']}'";
     $result = $db->query($sql);
     if($result && $db->affected_rows() === 1) {
       $session->msg("s", "Channels updated succesfully.");
       redirect('channels.php',false);
     } else {
       $session->msg("d", "Sorry! failed to update.");
       redirect('channels.php',false);
     }
  } else {
    $session->msg("d", $errors);
    redirect('channels.php',false);
  }
}
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
  <div class="row">
  <div class="col-md-3 pull-right noti">
  <?php echo display_msg($msg); ?>
</div>
  </div>
   <div class="row">
   <p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Edit Channels</b></p>
    <div class="col-md-3">
      <div class="panel panel-default">
        <div class="panel-body">
          <form method="post" action="edit_channels.php?id=<?php echo (int)$channels['id'];?>">
          <div class="form-group">
          <p>Channel name</p>
                <input type="text" class="form-control input-sm channels" name="channels-name" placeholder="Channels" require="required" value="<?php echo remove_junk(ucfirst($channels['name']));?>"> 
            </div>
            <button type="button" name="cancel" class="btn btn-default btn-sm pull-left" onclick="goBack();">Cancel</button>
            <button type="submit" name="edit_channels" class="btn btn-danger btn-sm btnupdate pull-right" disabled>Apply Changes</button>
        </form>
        </div>
      </div>
    </div>
   
   </div>
   <!-- <//?php include_once('layouts/customer_modal.php'); ?> -->
  </div>
  <?php include_once('layouts/footer.php'); ?>
  
  <script>
  $().ready(function() {
    //disbaled update button after any changes above inputs
    //
    $(".channels").focus( function(){
        $(".btnupdate").prop('disabled',false);
    });
  });
</script>
