<?php
  $page_title = 'DBEI | Edit Expense Account';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
?>
<?php
  //Display all desc.
  $exp_account = find_by_id('expenses_account',(int)$_GET['id']);
  if(!$exp_account){
    $session->msg("d","Missing code id.");
    redirect('expenses_acct.php');
  }
?>

<?php
if(isset($_POST['edit_exp_account'])){
  $req_field = array('expaccount-name');
  validate_fields($req_field);
  $expaccount_name = remove_junk($db->escape($_POST['expaccount-name']));
  $date = make_date();
  if(empty($errors)){
        $sql = "UPDATE expenses_account SET name='{$expaccount_name}', date_mod='{$date}'";
       $sql .= " WHERE id='{$exp_account['id']}'";
     $result = $db->query($sql);
     if($result && $db->affected_rows() === 1) {
       $session->msg("s", "Account updated succesfully.");
       redirect('expenses_acct.php',false);
     } else {
       $session->msg("d", "Sorry! failed to update.");
       redirect('expenses_acct.php',false);
     }
  } else {
    $session->msg("d", $errors);
    redirect('expenses_acct.php',false);
  }
}
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
<div class="row">
<div class="col-md-3 pull-right noti">
<?php echo display_msg($msg); ?>
</div><br>
<p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Edit Expenses Description</b></p>
   <div class="col-md-3">
     <div class="panel panel-default">
       <div class="panel-body">
         <form method="post" action="edit_expaccount.php?id=<?php echo (int)$exp_account['id'];?>">
           <div class="form-group">
           <p>Account name</p>
               <input type="text" class="form-control input-sm exp" name="expaccount-name" value="<?php echo remove_junk(ucfirst($exp_account['name']));?>">
           </div>
          <button type="button" name="cancel" class="btn btn-default btn-sm pull-left" onclick="goBack();">Cancel</button>
           <button type="submit" name="edit_exp_account" class="btn btn-danger btn-sm btnupdate pull-right" disabled>Apply Changes</button>
       </form>
       </div>
     </div>
   </div>
</div>



<?php include_once('layouts/footer.php'); ?>
<script>
  $(document).ready(function() {
    //disbaled update button after any changes above inputs
    //
    $(".exp").focus( function(){
        $(".btnupdate").prop('disabled',false);
    });
  });
</script>
