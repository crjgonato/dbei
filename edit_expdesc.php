<?php
  $page_title = 'DBEI | Edit Expenses Description';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
?>
<?php
  //Display all desc.
  $exp_desc = find_by_id('expenses_desc',(int)$_GET['id']);
  if(!$exp_desc){
    $session->msg("d","Missing code id.");
    redirect('expenses_desc.php');
  }
?>

<?php
if(isset($_POST['edit_exp_desc'])){
  $req_field = array('expdesc-name');
  validate_fields($req_field);
  $expdesc_name = remove_junk($db->escape($_POST['expdesc-name']));
  $date = make_date();
  if(empty($errors)){
      $sql = "UPDATE expenses_desc SET name='{$expdesc_name}', date_mod='{$date}'";
       $sql .= " WHERE id='{$exp_desc['id']}'";
     $result = $db->query($sql);
     if($result && $db->affected_rows() === 1) {
       $session->msg("s", "Description updated succesfully.");
       redirect('expenses_desc.php',false);
     } else {
       $session->msg("d", "Sorry! failed to update.");
       redirect('expenses_desc.php',false);
     }
  } else {
    $session->msg("d", $errors);
    redirect('expenses_desc.php',false);
  }
}
?>
<?php include_once('layouts/header.php'); ?>
<meta http-equiv="refresh" content="180" />
<div class="row">
<div class="col-md-3 pull-right noti">
<?php echo display_msg($msg); ?>
</div><br>
<p class="text-muted"> &nbsp;&nbsp;&nbsp;&nbsp;<b>Edit Expenses Description</b></p>
   <div class="col-md-3">
     <div class="panel panel-default">
       <div class="panel-body">
         <form method="post" action="edit_expdesc.php?id=<?php echo (int)$exp_desc['id'];?>">
           <div class="form-group">
           <p>Description</p>
               <input type="text" class="form-control input-sm exp" name="expdesc-name" value="<?php echo remove_junk(ucfirst($exp_desc['name']));?>">
           </div>
          <button type="button" name="cancel" class="btn btn-default btn-sm pull-left" onclick="goBack();">Cancel</button>
           <button type="submit" name="edit_exp_desc" class="btn btn-danger btn-sm btnupdate pull-right" disabled>Apply Changes</button>
       </form>
       </div>
     </div>
   </div>
</div>



<?php include_once('layouts/footer.php'); ?>
<script>
  $(document).ready(function() {
    //disbaled update button after any changes above inputs
    //
    $(".exp").focus( function(){
        $(".btnupdate").prop('disabled',false);
    });
  });
</script>
